from django.http.response import JsonResponse
from django.utils import timezone
from rest_framework.views import APIView
from rest_framework.request import Request
from common_files.read_logger import get_logger
from common_files.read_configuration import read_config
from common_files.create_response import create_failure, create_success
from common_files.create_connection import db_conn

from sql_queries.logs_query import session_details, current_login_details
from .models import Userlogindetails, Sessiondetails
from common_files.jwt_token import make_jwt_token, verify_jwt_token
import json
import requests
from rest_framework.request import HttpRequest, Request
import datetime
from .serializers import sessionSerializer
import pyodbc
import pandas as pd
import time, os, uuid
import ldap3
import base64
from dashboard.models import  Settings
from .serializers import settingsSerializer

# Create your views here.    
class login_api(APIView):
    def __init__(self):
        self.logger = get_logger()
        self.config = read_config()

    def get(self, request: Request) -> JsonResponse:
        """
        :param request:
        :return:
        """
        return JsonResponse({})
    
    def post(self, request: Request) -> JsonResponse:
        """
        :param request: Client request containing request information
        :return:
        """
        try:
            emailid = request.data['username']
            user_password = request.data['password']
        except Exception as e:
            self.logger.error("Error in email address or password."+str(e))
            return JsonResponse(create_failure(500, 'Value Missing', 'Fail'))
        if len(emailid) < 1:
            self.logger.error("Error in email address.")
            return JsonResponse(create_failure(401, 'Incorrect Email Address', 'Fail'))
        try:
            server_name = base64.b64decode(str(self.config['LDAP']['server_name'])).decode("utf-8")
            server = ldap3.Server(server_name,port=636,use_ssl=True)
            conn = ldap3.Connection(server, read_only=True, user=emailid, password=user_password, auto_bind=True)
            auth_response = self.user_authorization(emailid)
            if not auth_response[0]:
                self.logger.error("Issue with User AD Authorization:"+str(auth_response))
                self.user_authorization = 'Failed: User is not active!'
            else:
                self.user_authorization = 'Approve'

        except Exception as e:
            auth_response = self.user_authorization(emailid)
            if not auth_response[0]:
                self.logger.error("Issue with User AD Authorization:"+str(auth_response))
                self.user_authorization = 'Failed: User is not active!'
            else:  
                self.logger.error("Credentials are incorrect:"+str(e))
                self.user_authorization = 'Reject'
 
        try:
            userInfo = Userlogindetails.objects.get(id=emailid)
        except Exception as e:
            self.logger.error("Error in Email Address."+str(e))
            return JsonResponse(create_failure(401, 'Email not found', 'Fail'))
        try:
            if userInfo.status == 'Inactive' or userInfo.status == 'Dormant' or userInfo.status == 'Reject' or userInfo.status == 'Terminated':
                self.logger.error("User Not/De-active or Dormant")
                return JsonResponse(create_failure(401, 'User Not/De-active or Dormant', 'Fail'))
        except Exception as e:
            self.logger.error("User Not/De-active or Dormant")
            return JsonResponse(create_failure(401, 'User Not/De-active or Dormant', 'Fail'))
        try:
            if(self.user_authorization == 'Approve'):
                try:
                    try:
                        sessionObj = Sessiondetails.objects.filter(userid=userInfo.id,logouttime=None).order_by('-logintime')
                        self.logger.info(sessionObj[0])
                        previous_login=sessionObj[0]
                        previous_login.logouttime = datetime.datetime.now()
                        previous_login.save()
                        self.logger.info("Ran the query!")
                    except Exception as e:
                        self.logger.info("No logs found!")
                    sessionObj = Sessiondetails(userid=userInfo.id)
                    sessionID = sessionObj.pk
                    sessionObj.save()
                    jwtToken = make_jwt_token(data={'userid':userInfo.id})
                    outputDict = {
                        'Email': userInfo.emailaddress,
                        'Password': None,
                        'UserID': userInfo.id,
                        'Token': jwtToken,
                        "SessionID": sessionID,
                        "UserType":userInfo.usertype
                    }
                    output = create_success('Authorization Successful!', outputDict)
                except Exception as e:
                    self.logger.error("Error while connecting to POST API of Login Module."+str(e))
                    return JsonResponse(create_failure(401, 'Authorization Failed', 'Fail'))
            elif self.user_authorization == 'Reject':
                return JsonResponse(create_failure(401, 'Incorrect Password', 'Fail'))
            elif self.user_authorization == 'Failed: User is not active!':
                return JsonResponse(create_failure(401, 'Failed: User is not active!', 'Fail'))
            else:
                return JsonResponse(create_failure(500, 'Internal Error: Something went wrong', 'Fail'))
        except Exception as e:
            self.logger.error("Error while connecting to POST API of Login Module."+str(e))
            return JsonResponse(create_failure(401, 'Authorization Failed', 'Fail'))
        return JsonResponse(output)

    def user_authorization(self,emailid):
        return_status=[False,""]
        try:
            domain_name = emailid.split('\\')[0]
            employee_id = emailid.split('\\')[1]
            server_name = base64.b64decode(str(self.config['LDAP']['server_name'])).decode("utf-8")
            user_name = base64.b64decode(str(self.config['LDAP']['user_name'])).decode("utf-8")
            password = base64.b64decode(str(self.config['LDAP']['password'])).decode("utf-8")
            server = ldap3.Server(server_name, get_info=ldap3.ALL)
            conn = ldap3.Connection(server, user=user_name, password=password,auto_bind=True)
            try:
                query_set_ad = f'(&(objectclass=person)(sAMAccountName={employee_id}))'
                response = conn.search('dc={},dc=com'.format(domain_name), query_set_ad , attributes=[ldap3.ALL_ATTRIBUTES, ldap3.ALL_OPERATIONAL_ATTRIBUTES])
                user_data = conn.entries[0]
            except Exception as e:
                self.logger.error("User not found!"+str(e))
                return_status=[False,"User not found!"]
            try:
                if user_data.userAccountControl != 512:
                    return_status = [False,"Access Denied!"]
                else:
                    return_status = [True, "Success"]
            except Exception as e:
                return_status = [False, "Internal Error: userAccountControl Not Found!"]
            return return_status
        except Exception as e:
            self.logger.error("Something went wrong!"+str(e))
            return [False,'Authorization Failed: Please Check Input!']

class sso_login_api(APIView):
    def __init__(self):
        self.logger = get_logger()
    
    def post(self, request: Request) -> JsonResponse:
        """
        :param request: Client request containing request information
        :return:
        """
        try:
            token = request.data['Token']
        except Exception as e:
            self.logger.error("Error in Token."+str(e))
            return JsonResponse(create_failure(500, 'Value Missing', 'Fail'))

        try:
            headers = {"Authorization": "Bearer " + token }
            user_data = requests.get("https://graph.microsoft.com/v1.0/me",headers=headers).json()
        except Exception as e:
            self.logger.error("Error in Token."+str(e))
            return JsonResponse(create_failure(401, 'Invalid Token', 'Fail'))

        try:
            userInfo = Userlogindetails.objects.get(emailaddress=user_data['mail'])
        except Exception as e:
            self.logger.error("Error in finding user. " + str(e))
            return JsonResponse(create_failure(401, "User doesn't have access", "Fail"))

        try:
            if userInfo.isActive == False:
                self.logger.error("User is Inactive. ")
                return JsonResponse(create_failure(401, "User is Inactive. Please contact Admin", "Fail"))
            if userInfo.isapproved == False:
                self.logger.error("User is not Approved. ")
                return JsonResponse(create_failure(401, "User is not Approved", "Fail"))
        except Exception as e:
            self.logger.error("Error in finding user. " + str(e))
            return JsonResponse(create_failure(500, "Error in Login API", "Fail"))

        try:
            sessionObj = Sessiondetails(userid=userInfo.id)
            sessionID = sessionObj.pk
            sessionObj.save()
            jwtToken = make_jwt_token(data={'userid':userInfo.id})
            outputDict = {
                'Email': userInfo.emailaddress,
                'UserID': userInfo.id,
                'UserType': userInfo.usertype,
                'Token': jwtToken,
                "SessionID": sessionID
            }
            output = create_success('Authorization Successful!', outputDict)
        except Exception as e:
            self.logger.error("Error while connecting to POST API of Login Module."+str(e))
            return JsonResponse(create_failure(401, 'Authorization Failed', 'Fail'))
        return JsonResponse(output)


class logout_api(APIView):
    def __init__(self):
        self.logger = get_logger()
    
    def post(self, request: Request) -> JsonResponse:
        try:
            sessionID = request.data['SessionID']
        except Exception as e:
            self.logger.error("Error in Logout API: "+str(e))
            return JsonResponse(create_failure(400, 'Please provide valid sissionID', 'Fail'))

        try:
            sessionObj = Sessiondetails.objects.get(pk=sessionID)
            if sessionObj.logouttime != None:
                self.logger.error("Already logged out")
                return JsonResponse(create_failure(500, 'Already Logged out', 'Fail'))
            else:
                sessionObj.logouttime =datetime.datetime.now()
                sessionObj.save()
        except Exception as e:
            self.logger.error("Error in Logout API: "+str(e))
            return JsonResponse(create_failure(500, 'Failed to Logout', 'Fail'))
        return JsonResponse(create_success("Logout Successfully!", []))


class change_password(APIView):
    def __init__(self):
        self.logger = get_logger()

    def get(self, request: Request) -> JsonResponse:
        return JsonResponse({"Message": "Method Get Is Not Allowed While APi is working"})

    def post(self, request: Request) -> JsonResponse:
        try:
            userid = verify_jwt_token(request.META['HTTP_AUTHORIZATION'])
            if userid == -1:
                self.logger.error("Token is Expired")
                return JsonResponse(create_failure(401, 'Token is Expired', 'Fail'))
            elif userid == 0:
                self.logger.error("Error in changePassword API token: ")
                return JsonResponse(create_failure(400, 'invalid Token', 'Fail'))
        except Exception as e:
            self.logger.error("Error in changePassword API: "+str(e))
            return JsonResponse(create_failure(400, 'Please provide valid Token', 'Fail'))
        try:
            email = request.data['email']
            if not email:
                self.logger.error("invalid email")
                return JsonResponse(create_failure(400, 'provide valid email', 'Fail'))
        except Exception as e:
            self.logger.error("invalid email"+str(e))
            return JsonResponse(create_failure(400, 'provide email', 'Fail'))
        try:
            oldPassword = request.data['old_password']
            if not oldPassword:
                self.logger.error("invalid password")
                return JsonResponse(create_failure(400, 'provide valid old password', 'Fail'))
            userInfo = Userlogindetails.objects.get(emailaddress=email)
            if userInfo.password != (oldPassword):
                self.logger.error("invalid password")
                return JsonResponse(create_failure(400, 'invalid old password', 'Fail'))
        except Exception as e:
            self.logger.error("invalid password."+str(e))
            return JsonResponse(create_failure(400, 'provide valid old password', 'Fail'))
        try:
            newPassword = request.data['new_password']
            if not newPassword:
                self.logger.error("invalid new password")
                return JsonResponse(create_failure(400, 'provide valid new password', 'Fail'))
        except Exception as e:
            self.logger.error("invalid new password"+str(e))
            return JsonResponse(create_failure(400, 'provide valid new password', 'Fail'))
        try:
            if newPassword == oldPassword:
                self.logger.error("old password and new password should not be same")
                return JsonResponse(create_failure(400, 'old password and new password should not be same', 'Fail'))
        except Exception as e:
            self.logger.error("old password and new password should not be same"+str(e))
            return JsonResponse(create_failure(400, 'old password and new password should not be same', 'Fail'))
        try:
            currentTime = timezone.now()
            UserlogindetailsUpdate = Userlogindetails.objects.filter(
                emailaddress=email).update(password=newPassword, modifieddate=currentTime)
            if UserlogindetailsUpdate == 0:
                return JsonResponse(create_failure(204, 'Failed to update password', 'Fail'))
        except Exception as e:
            self.logger.error("invalid email" + str(e))
            return JsonResponse(create_failure(500, 'Failed to update password', 'Fail'))
        return JsonResponse(create_success("Password Updated Successfully!", []))


# Create your views here.
class get_login_details(APIView):
    def __init__(self):
        self.logger = get_logger()
        self.config = read_config()
 
    def post(self, request: Request) -> JsonResponse:
        try:
            userid = verify_jwt_token(request.META['HTTP_AUTHORIZATION'])
            if userid == -1:
                self.logger.error("Token is Expired")
                return JsonResponse(create_failure(401, 'Token is Expired', 'Fail'))
            elif userid == 0:
                self.logger.error("Error in get login details API token: ")
                return JsonResponse(create_failure(400, 'Invalid token', 'Fail'))
        except Exception as e:
            self.logger.error("Error in get login details API: "+str(e))
            return JsonResponse(create_failure(400, 'Please provide valid token', 'Fail'))
         
        try:
            try:
                self.user_id_search = request.data['user_id']
                self.session_id = request.data['session_id']
                self.from_date = request.data['from_date']
                self.to_date = self.get_to_date(request.data['to_date'])

            except Exception as e:
                self.logger.error("Error in payload : "+str(e))
                return JsonResponse(create_failure(500, 'Error in Payload', 'Fail')) 
            try:
                settingsObject = Settings.objects.all()
                settingsData = settingsSerializer(settingsObject, many=True) 
                if(settingsData.data):
                    try:
                        self.appUrl = settingsData.data[0]['appurl']
                        print(self.appUrl)
                    except Exception as e:
                        self.logger.error("Error in Settings Table "+str(e))
                        return create_failure(500, 'Error in Settings Table', 'Failed')
                else:
                    self.logger.error("Error in Settings Table "+str(e))
                    return create_failure(500, 'Error in Settings Table', 'Failed')

                connection = db_conn()
                script = session_details(self.from_date,self.to_date)
                self.logger.info(script)
                self.file_path = self.config['LOGS']['path']
                # self.unique_id = str(uuid.uuid4()) + str(time.time()) + '.xlsx'
                self.unique_id='Sign_in_Sign_out_User_History('+str(datetime.datetime.utcnow().strftime('%d-%m-%Y %H:%M:%S').replace(" ", "").replace("-", "").replace(":", "").replace(".", ""))+').xlsx'
                self.file_name = os.path.join(self.file_path, self.unique_id)
                self.logger.info(self.file_name)
                df = pd.read_sql_query(script, connection)
                print(df)
                if not os.path.exists(self.file_path):
                    os.makedirs(self.file_path)
                writer = pd.ExcelWriter(self.file_name)
                df.to_excel(writer, sheet_name='Sign_in_Sign_out_User_History',index=False)
                writer.save()
                download_url = self.appUrl + 'LogFiles/' + self.unique_id
                output = create_success('Log File URL', download_url)

            except Exception as e:
                self.logger.error("Error in get login details API: "+str(e))
                return JsonResponse(create_failure(500, 'get login details API Fail', 'Fail'))
            return JsonResponse(output)

        except Exception as e:
            self.logger.error("Error in get login details API: "+str(e))
            return JsonResponse(create_failure(400, 'get login details API Fail', 'Fail'))
        return JsonResponse(output)

    def get_to_date(self,to_date):
        if len(to_date) == 0:
            to_date = (timezone.now()).strftime("%Y-%m-%d")
        date_1 = datetime.datetime.strptime(to_date, "%Y-%m-%d")
        to_date = date_1 + datetime.timedelta(days=1)
        return to_date


class get_current_login_details(APIView):
    def __init__(self):
        self.logger = get_logger()
        self.config = read_config()
 
    def post(self, request: Request) -> JsonResponse:
        try:
            userid = verify_jwt_token(request.META['HTTP_AUTHORIZATION'])
            if userid == -1:
                self.logger.error("Token is Expired")
                return JsonResponse(create_failure(401, 'Token is Expired', 'Fail'))
            elif userid == 0:
                self.logger.error("Error in get login details API token: ")
                return JsonResponse(create_failure(400, 'Invalid token', 'Fail'))
        except Exception as e:
            self.logger.error("Error in get login details API: "+str(e))
            return JsonResponse(create_failure(400, 'Please provide valid token', 'Fail'))
         
        try:
            try:
                self.user_id_search = request.data['user_id']
                self.session_id = request.data['session_id']
                self.from_date = request.data['from_date']
                self.to_date = self.get_to_date(request.data['to_date'])
            except Exception as e:
                self.logger.error("Error in payload : "+str(e))
                return JsonResponse(create_failure(500, 'Error in Payload', 'Fail')) 
            
            try:
                settingsObject = Settings.objects.all()
                settingsData = settingsSerializer(settingsObject, many=True) 
                if(settingsData.data):
                    try:
                        self.appUrl = settingsData.data[0]['appurl']
                        print(self.appUrl)
                    except Exception as e:
                        self.logger.error("Error in Settings Table "+str(e))
                        return create_failure(500, 'Error in Settings Table', 'Failed')
                else:
                    self.logger.error("Error in Settings Table "+str(e))
                    return create_failure(500, 'Error in Settings Table', 'Failed')

                connection = db_conn()
                script = current_login_details(self.from_date,self.to_date)
                self.logger.info(script)
                self.file_path = self.config['LOGS']['path']
                # self.unique_id = str(uuid.uuid4()) + str(time.time()) + '.xlsx'
                self.unique_id='Current_Login_History('+str(datetime.datetime.utcnow().strftime('%d-%m-%Y %H:%M:%S').replace(" ", "").replace("-", "").replace(":", "").replace(".", ""))+').xlsx'
                self.file_name = os.path.join(self.file_path, self.unique_id)
                self.logger.info(self.file_name)
                df = pd.read_sql_query(script, connection)
                if not os.path.exists(self.file_path):
                    os.makedirs(self.file_path)
                writer = pd.ExcelWriter(self.file_name)
                df.to_excel(writer, sheet_name='Current_Login_History', index=False)
                writer.save()
                download_url = self.appUrl + 'LogFiles/' + self.unique_id
                output = create_success('Log File URL', download_url)

            except Exception as e:
                self.logger.error("Error in get login details API: "+str(e))
                return JsonResponse(create_failure(500, 'get login details API Fail', 'Fail'))
            return JsonResponse(output)

        except Exception as e:
            self.logger.error("Error in get login details API: "+str(e))
            return JsonResponse(create_failure(400, 'get login details API Fail', 'Fail'))
        return JsonResponse(output)

    def get_to_date(self,to_date):
        if len(to_date) == 0:
            to_date = (timezone.now()).strftime("%Y-%m-%d")
        date_1 = datetime.datetime.strptime(to_date, "%Y-%m-%d")
        to_date = date_1 + datetime.timedelta(days=1)
        return to_date