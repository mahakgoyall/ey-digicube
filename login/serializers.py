from rest_framework import serializers
from .models import Sessiondetails
from dashboard.models import Settings
class sessionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Sessiondetails
        fields = '__all__'

class settingsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Settings
        fields = '__all__'
