from django.urls import  path
from . import views

urlpatterns = [
    path('auth/login', views.login_api.as_view()),
    path('auth/logout', views.logout_api.as_view()),
    path('auth/change-password', views.change_password.as_view()),
    path('auth/login-history', views.get_login_details.as_view()),
    path('auth/current-login-history', views.get_current_login_details.as_view())

]