from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from login.models import Userlogindetails
from django.test import Client
import json 

class AccountTests(APITestCase):
    def setUp(self):
        # Every test needs a client.
        self.client = Client()
        Userlogindetails.objects.create(id = 'U-1', loginid = b'0x307832434330304444444446444343383143343238444433434635414643', password = '123456', emailaddress = 'vikash@test.com', mbsn = b'0x307835384539433639393334344646374234433842313146464244413133')
        # and so on. for each prerequisite that should be there in db
    
    # Both Email and PAssword are correct
    def test1(self):
        """
        Ensure we can create a new account object.
        """
        data = {"email":"vikash@test.com","password":"123456"}
        response = self.client.post("/v1/auth/login",data,format = 'json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), {
            "statusCode": 200,
            "message": "Authorization Successful!",
            "replyCode": "Success",
            "data": {
                "email": "vikash@test.com",
                "password": "123456",
                "userID": "U-1",
                "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyaWQiOiJVLTEifQ.aRzeeoMrtxJIi9HB4_9YoVnzJ16tgM0Xux5_q6q0gh0"
            }
        })

    # When Email Address is incorrect
    def test2(self):
        data = {"email":"vikkash@test.com","password":"123456"}
        response = self.client.post("/v1/auth/login",data,format = 'json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), {
            "statusCode": 401,
            "message": "Email not found",
            "replyCode": "Fail",
            "data": {}
        })

    # When Incorrect Password
    def test3(self):
        data = {"email":"vikash@test.com","password":"1234561"}
        response = self.client.post("/v1/auth/login",data,format = 'json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), {
            "statusCode": 401,
            "message": "Incorrect Password",
            "replyCode": "Fail",
            "data": {}
        })
  
    # When Incorrect Password
    def test4(self):
        data = {"email":"vikash@test.com","password":"1234567"}
        response = self.client.post("/v1/auth/login",data,format = 'json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), {
            "statusCode": 401,
            "message": "Incorrect Password",
            "replyCode": "Fail",
            "data": {}
        })
  
    # When both Email address and password are incorrect
    def test5(self):
        data = {"email":"vikaash@test.com","password":"1234561"}
        response = self.client.post("/v1/auth/login",data,format = 'json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), {
            "statusCode": 401,
            "message": "Email not found",
            "replyCode": "Fail",
            "data": {}
        })
  
    # When Password key is not provided
    def test6(self):
        data = {"email":"vikash@test.com"}
        response = self.client.post("/v1/auth/login",data,format = 'json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), {
            "statusCode": 500,
            "message": "Value Missing",
            "replyCode": "Fail",
            "data": {}
        })

    # When Email Key is not provided
    def test7(self):
        data = {"password":"123456"}
        response = self.client.post("/v1/auth/login",data,format = 'json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), {
            "statusCode": 500,
            "message": "Value Missing",
            "replyCode": "Fail",
            "data": {}
        })

    # Null Email Field
    def test7(self):
        data = {"email":"","password":"123456"}
        response = self.client.post("/v1/auth/login",data,format = 'json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), {
            "statusCode": 401,
            "message": "Incorrect Email Address",
            "replyCode": "Fail",
            "data": {}
        })