def session_details(from_date,to_date):
    query = "select SessionID,LoginTime,LogoutTime,UserID from [dbo].[UserSessions] where LoginTime between '{}' and '{}'".format(from_date,to_date)
    return query

def current_login_details(from_date,to_date):
    query = "select SessionID,LoginTime,LogoutTime,UserID from [dbo].[UserSessions] where LogoutTime is NULL and LoginTime between '{}' and '{}'".format(from_date,to_date)
    return query

def created_user_history(from_date,to_date):
    query = "select * from [dbo].[UserLoginDetailsLog] where Command='Insert' and ModifiedDate between '{}' and '{}'".format(from_date,to_date)
    return query

def update_user_history(from_date,to_date):
    query = "select * from [dbo].[UserLoginDetailsLog] where Command='Update' and ModifiedDate between '{}' and '{}'".format(from_date,to_date)
    return query

def InActive_user_history(from_date,to_date):
    query = "select * from [dbo].[UserLoginDetailsLog] where Status = 'InActive' and IsApproved = 1 and ModifiedDate between '{}' and '{}'".format(from_date,to_date)
    return query