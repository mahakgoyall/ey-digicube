def insert_record_in_tbl_m_database_table(databasesettingsid,databasename,databasetype,servername,username,iswinauthentication,publisheddatetime,password):
    query = "INSERT INTO tbl_m_DatabaseSettings (ID,DatabaseName, DatabaseType, ServerName, UserName, IsWinAuthentication, PublishedDateTime, Password) VALUES ('"+databasesettingsid+"','"+databasename+"','"+databasetype+"','"+servername+"','"+username+"','"+iswinauthentication+"','"+publisheddatetime+"',"+"CONVERT(VARBINARY(300), '"+password+"')"+")"
    return query

def get_uat_new_session():
    return [
'''
/****** Object:  Table [dbo].[Death_Claim_Data]    Script Date: 10/1/2020 1:03:21 AM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[Death_Claim_Data](
	[ID] [varchar](50) NULL,
	[SessionID] [varchar](50) NULL,
	[ClientCode] [varchar](255) NULL,
	[CreatedBy] [varchar](255) NULL,
	[CreatedDate] [date] NULL,
	[ModifiedBy] [varchar](255) NULL,
	[ModifiedDate] [datetime] NULL,
	[System_Name] [nvarchar](max) NULL,
	[NAIC_number] [nvarchar](max) NULL,
	[Policy_Number] [nvarchar](max) NULL,
	[Product_Type] [nvarchar](max) NULL,
	[Product_Type_Description] [nvarchar](max) NULL,
	[Issue_State] [nvarchar](max) NULL,
	[Resident_State] [nvarchar](max) NULL,
	[Issue_Date] [nvarchar](max) NULL,
	[Policy_Date] [nvarchar](max) NULL,
	[Policy_Face_Amount] [nvarchar](max) NULL,
	[Serv_Rep_Contr_No] [nvarchar](max) NULL,
	[Insured_First_Name] [nvarchar](max) NULL,
	[Insured_Middle_Name] [nvarchar](max) NULL,
	[Insured_Last_Name] [nvarchar](max) NULL,
	[Date_of_Death] [nvarchar](max) NULL,
	[Reason_Claim_Denial] [nvarchar](max) NULL,
	[Denied_Claim_Date] [nvarchar](max) NULL,
	[Transaction] [nvarchar](max) NULL,
	[Agency_Code] [nvarchar](max) NULL,
	[Payment_Date] [nvarchar](max) NULL,
	[Date_Claim_Received] [nvarchar](max) NULL,
	[Interest_Paid] [nvarchar](max) NULL,
	[Check_Amount] [nvarchar](max) NULL,
	[Payee_Name] [nvarchar](max) NULL,
	[Payee_State] [nvarchar](max) NULL,
	[Check_Number] [nvarchar](max) NULL,
	[Kind_Code] [nvarchar](max) NULL,
	[ClaimPaid_in_60_Days_DOD] [nvarchar](max) NULL,
	[Cash_Value_Prod_Indicator] [nvarchar](max) NULL,
	[Policy_Status] [nvarchar](max) NULL,
	[Policy_Status_Change_date] [nvarchar](max) NULL,
	[sdf_Issue_Date] [date] NULL,
	[sdf_Policy_Date] [date] NULL,
	[sdf_Policy_Face_Amount] [float] NULL,
	[sdf_Date_of_Death] [date] NULL,
	[sdf_Denied_Claim_Date] [date] NULL,
	[sdf_Payment_Date] [date] NULL,
	[sdf_Interest_Paid] [float] NULL,
	[sdf_Check_Amount] [float] NULL,
	[sdf_Date_Claim_Received] [date] NULL,
	[sdf_CHEQUE_NUMBER_COL] [varchar](50) NULL,
	[sdf_Bucket_ClaimReceived_PaymentDate] [varchar](50) NULL,
	[sdf_Bucket_DeathDate_PaymentDate] [varchar](50) NULL,
	[sdf_Beneficiary_type] [nvarchar](20) NULL,
	[sdf_Policy_Status_Change_date] [date] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


/****** Object:  Table [dbo].[Payment_Data]    Script Date: 10/1/2020 1:03:22 AM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[Payment_Data](
	[ID] [varchar](50) NULL,
	[SessionID] [varchar](50) NULL,
	[ClientCode] [varchar](255) NULL,
	[CreatedBy] [varchar](255) NULL,
	[CreatedDate] [date] NULL,
	[ModifiedBy] [varchar](255) NULL,
	[ModifiedDate] [datetime] NULL,
	[Company] [nvarchar](max) NULL,
	[Contract_Number] [nvarchar](max) NULL,
	[Payee_Number] [nvarchar](max) NULL,
	[Payee_Name] [nvarchar](max) NULL,
	[Payment_Source] [nvarchar](max) NULL,
	[Money_Type] [nvarchar](max) NULL,
	[Check_Date] [nvarchar](max) NULL,
	[Check_Number] [nvarchar](max) NULL,
	[Gross_Amount] [nvarchar](max) NULL,
	[Net_Amount] [nvarchar](max) NULL,
	[Check_Amount] [nvarchar](max) NULL,
	[Check_Status] [nvarchar](max) NULL,
	[Statement_Address3] [nvarchar](max) NULL,
	[sdf_Check_Date] [date] NULL,
	[sdf_Check_Amount] [float] NULL,
	[sdf_Gross_Amount] [float] NULL,
	[sdf_Net_Amount] [float] NULL,
	[sdf_Check_Number] [nvarchar](50) NULL,
	[sdf_Beneficiary_type] [nvarchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

/****** Object:  Table [dbo].[SP_TABLE_TEST1_Detail]    Script Date: 10/1/2020 1:03:22 AM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[SP_TABLE_TEST1_Detail](
	[System_Name] [nvarchar](max) NULL,
	[NAIC_number] [nvarchar](max) NULL,
	[Policy_Number] [nvarchar](max) NULL,
	[Product_Type] [nvarchar](max) NULL,
	[Product_Type_Description] [nvarchar](max) NULL,
	[Issue_State] [nvarchar](max) NULL,
	[Resident_State] [nvarchar](max) NULL,
	[Issue_Date] [nvarchar](max) NULL,
	[Cleaned_Issue_Date] [date] NULL,
	[Policy_Date] [nvarchar](max) NULL,
	[Cleaned_Policy_Date] [date] NULL,
	[Policy_Face_Amount] [nvarchar](max) NULL,
	[Cleaned_Policy_Face_Amount] [float] NULL,
	[Serv_Rep_Contr_No] [nvarchar](max) NULL,
	[Insured_First_Name] [nvarchar](max) NULL,
	[Insured_Middle_Name] [nvarchar](max) NULL,
	[Insured_Last_Name] [nvarchar](max) NULL,
	[Date_of_Death] [nvarchar](max) NULL,
	[Cleaned_Date_of_Death] [date] NULL,
	[Reason_Claim_Denial] [nvarchar](max) NULL,
	[Denied_Claim_Date] [nvarchar](max) NULL,
	[Cleaned_Denied_Claim_Date] [date] NULL,
	[Transaction] [nvarchar](max) NULL,
	[Agency_Code] [nvarchar](max) NULL,
	[Payment_Date] [nvarchar](max) NULL,
	[Cleaned_Payment_Date] [date] NULL,
	[Date_Claim_Received] [nvarchar](max) NULL,
	[Cleaned_Date_Claim_Received] [date] NULL,
	[Interest_Paid] [nvarchar](max) NULL,
	[Cleaned_Interest_Paid] [float] NULL,
	[Check_Amount] [nvarchar](max) NULL,
	[Cleaned_Check_Amount] [float] NULL,
	[Payee_Name] [nvarchar](max) NULL,
	[Payee_State] [nvarchar](max) NULL,
	[Check_Number] [nvarchar](max) NULL,
	[Cleaned_Check_Number] [varchar](50) NULL,
	[Kind_Code] [nvarchar](max) NULL,
	[ClaimPaid_in_60_Days_DOD] [nvarchar](max) NULL,
	[Cash_Value_Prod_Indicator] [nvarchar](max) NULL,
	[Policy_Status] [nvarchar](max) NULL,
	[Beneficiary_type] [nvarchar](20) NULL,
	[Bucket_ClaimReceived_PaymentDate] [varchar](50) NULL,
	[Bucket_DeathDate_PaymentDate] [varchar](50) NULL,
	[Policy_Status_Change_date] [nvarchar](max) NULL,
	[Cleaned_Policy_Status_Change_date] [date] NULL,
	[Duplicate_Count] [int] NULL,
	[Count_of_Trans_Type] [int] NULL,
	[Transaction_Type] [varchar](3) NULL,
	[Total_of_Check_Amt] [float] NULL,
	[Total_of_Interest_Amt] [float] NULL,
	[Net_Check_Amt] [float] NULL,
	[Diff_Tot_check_amt_Int_amt_face_amt] [float] NULL,
	[Max_of_Check_Amt] [float] NULL,
	[Min_of_Check_Amt] [float] NULL,
	[Duplicate_Flag] [varchar](3) NULL,
	[Payment_check_status] [nvarchar](max) NULL,
	[SessionID] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

/****** Object:  Table [dbo].[SP_TABLE_TEST1_Summary]    Script Date: 10/1/2020 1:03:22 AM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[SP_TABLE_TEST1_Summary](
	[System_Name] [nvarchar](max) NULL,
	[Policy_Number] [nvarchar](max) NULL,
	[INSURED_NAME] [nvarchar](max) NULL,
	[sdf_Date_of_Death] [date] NULL,
	[sdf_Policy_Face_Amount] [float] NULL,
	[Payee_Name] [nvarchar](max) NULL,
	[Duplicate_Count] [int] NULL,
	[Count_of_Trans_Type] [int] NULL,
	[Transaction_Type] [varchar](3) NOT NULL,
	[Total_of_Check_Amt] [float] NULL,
	[Total_of_Interest_Amt] [float] NULL,
	[Net_Check_Amt] [float] NULL,
	[Diff_Tot_check_amt_Int_amt_face_amt] [float] NULL,
	[Max_of_Check_Amt] [float] NULL,
	[Min_of_Check_Amt] [float] NULL,
	[SessionID] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

''',
'''
/****** Object:  View [dbo].[View_TEST1_Summ]    Script Date: 10/1/2020 1:03:22 AM ******/
CREATE VIEW [dbo].[View_TEST1_Summ] AS 
	SELECT 
	[SessionID]						as 'SessionID',
	[System_Name]					AS 'V_System_Name',
	[Policy_Number]					AS 'V_Policy_Number',
	[Insured_First_Name]+[Insured_Middle_Name]+[Insured_Last_Name] AS 'V_INSURED_NAME',
	[sdf_Date_of_Death]				AS 'V_sdf_Date_of_Death',
	[sdf_Policy_Face_Amount]		AS 'V_sdf_Policy_Face_Amount',
	[Payee_Name]					AS 'V_Payee_Name',
	COUNT(*)						AS 'V_Duplicate_Count',
	COUNT(DISTINCT([TRANSACTION]))  AS 'v_Count_of_Trans_Type',
	CASE 
	WHEN COUNT(DISTINCT([TRANSACTION])) = 1 THEN 'Yes'
	ELSE 'No'
	END as 'V_Transaction_Type',
	SUM ([sdf_Check_Amount]) as 'V_Tot_of_Check_Amt',
	sum ([sdf_Interest_Paid]) as 'V_Tot_of_Interest_Amt',
	(sum (sdf_Check_Amount)-sum ([sdf_Interest_Paid])) AS 'V_Net_Check_Amt',
	(sum (sdf_Check_Amount)-sum ([sdf_Interest_Paid]))-[sdf_Policy_Face_Amount] AS 'V_Diff_Tot_check_amt_Int_amt_face_amt',
	MAX(sdf_Check_Amount) AS 'V_Max_of_Check_Amt' ,
	MIN(sdf_Check_Amount) AS 'V_Min_of_Check_Amt',
	CASE WHEN COUNT(*) > 1 THEN 'Yes' else 'No' end as 'Duplicate_Flag'
	FROM Death_Claim_Data
	GROUP BY
	[SessionID],
	[System_Name],
	[Policy_Number],
	[Insured_First_Name]+[Insured_Middle_Name]+[Insured_Last_Name],
	[sdf_Date_of_Death],
	[sdf_Policy_Face_Amount],
	[Payee_Name]

''',
'''
/****** Object:  StoredProcedure [dbo].[sp_Test_1a]    Script Date: 10/1/2020 1:03:22 AM ******/

CREATE PROCEDURE [dbo].[sp_Test_1a]
	@IsSuccessfull bit = 0 output
AS
BEGIN
	SET NOCOUNT ON;
	begin try

--------------------------------

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SP_TABLE_TEST1_Summary'))
TRUNCATE TABLE SP_TABLE_TEST1_Summary

INSERT INTO [dbo].[SP_TABLE_TEST1_Summary]
SELECT 
	[V_System_Name] as 'System_Name'
	,[V_Policy_Number] as 'Policy_Number'
	,[V_INSURED_NAME] as 'INSURED_NAME'
	,[V_sdf_Date_of_Death] as 'sdf_Date_of_Death'
	,[V_sdf_Policy_Face_Amount] as 'sdf_Policy_Face_Amount'
	,[V_Payee_Name] as 'Payee_Name'
	,[V_Duplicate_Count] as 'Duplicate_Count'
	,[v_Count_of_Trans_Type] as 'Count_of_Trans_Type'
	,[V_Transaction_Type] as 'Transaction_Type'
	,[V_Tot_of_Check_Amt] as 'Total_of_Check_Amt'
	,[V_Tot_of_Interest_Amt] as 'Total_of_Interest_Amt'
	,[V_Net_Check_Amt] as 'Net_Check_Amt'
	,[V_Diff_Tot_check_amt_Int_amt_face_amt] as 'Diff_Tot_check_amt_Int_amt_face_amt'
	,[V_Max_of_Check_Amt] as 'Max_of_Check_Amt'
	,[V_Min_of_Check_Amt] as 'Min_of_Check_Amt'
	,SessionID 
FROM [View_TEST1_Summ] t1
WHERE Duplicate_Flag = 'Yes'

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SP_TABLE_TEST1_Detail'))
--drop table SP_TABLE_TEST1_Detail


truncate table SP_TABLE_TEST1_Detail

insert into SP_TABLE_TEST1_Detail
SELECT t1.[System_Name]
,t1.[NAIC_number]
,t1.[Policy_Number]
,t1.[Product_Type]
,t1.[Product_Type_Description]
,t1.[Issue_State]
,t1.[Resident_State]
,t1.[Issue_Date]
,t1.[sdf_Issue_Date] AS 'Cleaned_Issue_Date'
,t1.[Policy_Date]
,t1.[sdf_Policy_Date] AS 'Cleaned_Policy_Date'
,t1.[Policy_Face_Amount]
,t1.[sdf_Policy_Face_Amount] AS 'Cleaned_Policy_Face_Amount'
,t1.[Serv_Rep_Contr_No]
,t1.[Insured_First_Name]
,t1.[Insured_Middle_Name]
,t1.[Insured_Last_Name]
,t1.[Date_of_Death]
,t1.[sdf_Date_of_Death] AS 'Cleaned_Date_of_Death'
,t1.[Reason_Claim_Denial]
,t1.[Denied_Claim_Date]
,t1.[sdf_Denied_Claim_Date] AS 'Cleaned_Denied_Claim_Date'
,t1.[Transaction]
,t1.[Agency_Code]
,t1.[Payment_Date]
,t1.[sdf_Payment_Date] AS 'Cleaned_Payment_Date'
,t1.[Date_Claim_Received]
,t1.[sdf_Date_Claim_Received] AS 'Cleaned_Date_Claim_Received'
,t1.[Interest_Paid]
,t1.[sdf_Interest_Paid] AS 'Cleaned_Interest_Paid'
,t1.[Check_Amount]
,t1.[sdf_Check_Amount] AS 'Cleaned_Check_Amount'
,t1.[Payee_Name]
,t1.[Payee_State]
,t1.[Check_Number]
,t1.[sdf_CHEQUE_NUMBER_COL] AS 'Cleaned_Check_Number'
,t1.[Kind_Code]
,t1.[ClaimPaid_in_60_Days_DOD]
,t1.[Cash_Value_Prod_Indicator]
,t1.[Policy_Status] 
,t1.[sdf_Beneficiary_type] as 'Beneficiary_type'
,t1.[sdf_Bucket_ClaimReceived_PaymentDate] as 'Bucket_ClaimReceived_PaymentDate'
,t1.[sdf_Bucket_DeathDate_PaymentDate] as 'Bucket_DeathDate_PaymentDate'
,t1.[Policy_Status_Change_date]
,t1.[sdf_Policy_Status_Change_date] as 'Cleaned_Policy_Status_Change_date'
,View_TEST1_Summ.[V_Duplicate_Count] as 'Duplicate_Count'
,View_TEST1_Summ.[v_Count_of_Trans_Type] as 'Count_of_Trans_Type'
,View_TEST1_Summ.[V_Transaction_Type] as 'Transaction_Type'
,View_TEST1_Summ.[V_Tot_of_Check_Amt] as 'Total_of_Check_Amt'
,View_TEST1_Summ.[V_Tot_of_Interest_Amt] as 'Total_of_Interest_Amt'
,View_TEST1_Summ.[V_Net_Check_Amt] as 'Net_Check_Amt'
,View_TEST1_Summ.[V_Diff_Tot_check_amt_Int_amt_face_amt] as 'Diff_Tot_check_amt_Int_amt_face_amt'
,View_TEST1_Summ.[V_Max_of_Check_Amt] as 'Max_of_Check_Amt'
,View_TEST1_Summ.[V_Min_of_Check_Amt] as 'Min_of_Check_Amt'
,View_TEST1_Summ.[Duplicate_Flag] as 'Duplicate_Flag'
,Payment_Data.Check_Status as 'Payment_check_status'
,t1.SessionID
FROM Death_Claim_Data t1
left JOIN View_TEST1_Summ
ON 
[System_Name]				= [V_System_Name] AND
[Policy_Number]				= [V_Policy_Number] AND
[Insured_First_Name]+[Insured_Middle_Name]+[Insured_Last_Name] = [V_INSURED_NAME] AND
[sdf_Date_of_Death]			= [V_sdf_Date_of_Death] AND
[sdf_Policy_Face_Amount]	= [V_sdf_Policy_Face_Amount] AND
[Payee_Name]				= [V_Payee_Name]
LEFT JOIN Payment_Data
ON 
t1.[sdf_CHEQUE_NUMBER_COL] = Payment_Data.[sdf_Check_Number]
AND t1.[System_Name] = LEFT(Payment_Data.[COMPANY],3)
AND t1.[Policy_Number] = Payment_Data.[Contract_Number]





--------------------------------	

	set @IsSuccessfull=1;
	end try

	begin catch 
	set @IsSuccessfull=0;
	end catch
  
select @IsSuccessfull as 'is_successfull'

END

''',
'''
/****** Object:  StoredProcedure [dbo].[sp_Test_2a]    Script Date: 10/1/2020 1:03:22 AM ******/


CREATE PROCEDURE [dbo].[sp_Test_2a]
	@IsSuccessfull bit = 0 output
AS
BEGIN
	SET NOCOUNT ON;
	begin try
	
SELECT 
SessionID,
LEFT(LTRIM([COMPANY]),3) AS 'SYSTEM',
[Contract_Number] AS 'CONTRACT_NO',
[Payee_Name] AS 'PAYEE_NAME1',
[sdf_Check_Date] AS 'CHECK_DATE1',
[sdf_Beneficiary_type] AS 'BENEFICIARY TYPE',
COUNT(*) AS 'Split Count'
INTO #SP_TABLE_TEST4_2_1_Summary_Split
FROM [Payment_Data]
GROUP BY
SessionID,
LEFT(LTRIM([COMPANY]),3), 
[Contract_Number], 
[Payee_Name],
[sdf_Beneficiary_type],
[sdf_Check_Date]

SELECT B.*,A.[SDF_POLICY_FACE_AMOUNT] AS 'face_amount',
A.[sdf_Date_Claim_Received] AS 'Date_Claim_Received',
A.[sdf_Payment_Date] AS 'Payment_Date'
into #Test4_data
FROM Payment_Data AS B 
LEFT JOIN Death_Claim_Data AS A
ON
ltrim(rtrim(A.[sdf_CHEQUE_NUMBER_COL])) = Ltrim(Rtrim(B.[sdf_Check_Number]))
AND ltrim(A.[System_Name]) = LEFT(LTRIM(B.[Company]),3)
AND ltrim(A.[Policy_Number]) = ltrim(B.[Contract_Number])

SELECT 
sessionid,
LEFT(LTRIM([COMPANY]),3) AS 'SYSTEM1',
[Contract_Number] AS 'CONTRACT_NO1',
[Payee_Name] AS 'PAYEE_NAME1',
[sdf_net_Amount] AS 'NET_AMT1',
[sdf_Check_Date] AS 'CHECK_DATE1',
[FACE_AMOUNT] AS 'FACE_AMOUNT1',
[sdf_Beneficiary_type] AS 'BENEFICIARY TYPE',
COUNT(*) AS 'DUPLICATE COUNT'
INTO #SP_TABLE_TEST4_2_1_Summary_Duplicate
FROM #Test4_data
GROUP BY
sessionid,
LEFT(LTRIM([COMPANY]),3), 
[Contract_Number], 
[Payee_Name],
[sdf_net_Amount], 
[sdf_Check_Date],
[FACE_AMOUNT],
[sdf_Beneficiary_type]


select A.*, B.[Split Count], C.[DUPLICATE COUNT]
into #SP_TABLE_TEST4_2_2_Output
FROM [dbo].[Payment_Data] AS A
LEFT JOIN #SP_TABLE_TEST4_2_1_Summary_Split AS B
ON LEFT(LTRIM(A.[COMPANY]),3) = B.SYSTEM AND A.[Contract_Number] = B.CONTRACT_NO AND A.[Payee_Name] = B.PAYEE_NAME1 AND  A.[sdf_Check_Date]  = B.CHECK_DATE1 AND  A.[sdf_Beneficiary_type]  = B.[Beneficiary type]
LEFT join #SP_TABLE_TEST4_2_1_Summary_Duplicate as c
on LEFT(LTRIM(A.[COMPANY]),3) = c.SYSTEM1 and A.[Contract_Number] = C.CONTRACT_NO1 and A.[Payee_Name] = C.PAYEE_NAME1 AND A.[sdf_net_Amount] = C.[NET_AMT1] AND A.[sdf_Check_Date] = C.[CHECK_DATE1]

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SP_TABLE_TEST4_2_1_Summary_Split'))
drop table SP_TABLE_TEST4_2_1_Summary_Split

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SP_TABLE_TEST4_2_2_Output'))
drop table SP_TABLE_TEST4_2_2_Output

select [SYSTEM] as 'System_Name'
      ,[CONTRACT_NO] as 'Contract_No'
      ,[PAYEE_NAME1] as 'Payee_Name'
      ,[CHECK_DATE1] as 'Check_Date'
      ,[BENEFICIARY TYPE] 
      ,[Split Count] as 'Split Count'
	  ,sessionid
into SP_TABLE_TEST4_2_1_Summary_Split from #SP_TABLE_TEST4_2_1_Summary_Split where [Split Count] > 1

select 
CASE 
WHEN [Split Count] = 1 AND [Duplicate Count] = 1 THEN 'No Exception'
WHEN [Split Count] > 1 AND [Duplicate Count] > 1 THEN 'Split with Duplicate'
WHEN [Split Count] > 1 AND [Duplicate Count] < 2 THEN 'Split without Duplicate'
ELSE 'Duplicate with Split*'
END AS 'Exception_Flag'
	  ,left([Company],3) as 'System_Name'
      ,[Company]
	  ,[Contract_Number]
      ,[Payee_Number]
      ,[Payee_Name]
      ,[Payment_Source]
      ,[Money_Type]
      ,[Check_Date]
	  ,[sdf_Check_Date] as 'Cleaned_Check_Date'
      ,[Check_Number]
      ,[sdf_Check_Number] as 'Cleaned_Check_Number'
      ,[Gross_Amount]
      ,[sdf_Gross_Amount] as 'Cleaned_Gross_Amount'
      ,[Net_Amount]
      ,[sdf_Net_Amount] as 'Cleaned_Net_Amount'
      ,[Check_Amount]
      ,[sdf_Check_Amount] as 'Cleaned_Check_Amount'
      ,[Check_Status]
      ,[Statement_Address3]
	  ,[sdf_Beneficiary_type] as 'Beneficiary_type'
      ,[Split Count]
      ,[DUPLICATE COUNT]
	  ,SessionID
into SP_TABLE_TEST4_2_2_Output
from #SP_TABLE_TEST4_2_2_Output


	set @IsSuccessfull=1;
	end try

	begin catch 
	set @IsSuccessfull=0;
	end catch
  
select @IsSuccessfull 

END
''',
    ]