def fetch_users():
    query = "select ID from [dbo].[UserLoginDetails] where UserType = 'Normal' and isActive = 1"
    return query

def fetch_sessions(user_id):
    query = "select top 1 cast(LoginTime as date) from [dbo].[SessionDetails] where UserID = '{0}' order by LoginTime desc".format(user_id)
    return query    

def update_logindetails(user_id):
    query = "update [dbo].[UserLoginDetails] set Status = 'Dormant', IsActive = 0, ReasonOfChange = 'Not Login from Past 90 Days' where ID = '{0}'".format(user_id)
    return query

def user_settings():
    query = "select DormancyTime from [dbo].[UserSettings]"
    return query

def fetch_all_users():
    query = "select ID,status from [dbo].[UserLoginDetails] where status in ('Active','Terminated')"
    return query

def update_user_status(id,isactive,status):
    query = f"update [dbo].[UserLoginDetails] set isactive = '{isactive}', status = '{status}', ReasonofChange = 'Cron - Terminated - AD effect' where [ID] = '{id}'"
    return query