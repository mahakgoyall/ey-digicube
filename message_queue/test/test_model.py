from django.test import TestCase
from event_emitter.models import Todolist

class TestModels(TestCase):
    
    def setUp(self):
        # Todolist 1
        self.Todolist = Todolist.objects.create(id = '01', eventtype = 'Triggered',eventid='01',todotypeid='01')

    def test_Todolist_models(self):
        objects = Todolist.objects.get(id = '01')
        self.assertEqual(objects.id, '01')
        self.assertEqual(objects.eventtype, 'Triggered')
        self.assertEqual(objects.eventid, '01')
        self.assertEqual(objects.todotypeid, '01')