from common_files.read_configuration import read_config	
from common_files.jwt_token import make_jwt_token, extract_jwt_info, verify_jwt_token
from common_files.read_logger import get_logger
from common_files.create_response import create_failure_modified, create_success_modified, create_failure, create_success
from common_files.create_connection import db_conn

from django.core.serializers.json import DjangoJSONEncoder

import requests,json,csv,os,io,base64,hashlib,urllib
import pandas as pd
import datetime
from dateutil import parser
import time
import numpy as np


from dashboard.models import Excelimportmappingmain, ExcelImportMappingDetail
from sql_queries.upload_file_sql_query import upload_file_sql_query
from .serializers import excelimportmappingmainSerializer, excelimportmappingdetailSerializer

########################################################################################################
# dump_file - is the main function. 
# parse_csv_file - is used to read csv file from the server and process it.
# parse_excel_file - is used to read excel file from the server and process it.
# insert_into_db - is used to run iteract with Database and execute pyodbc queries(insert)
# table_column_mapping - is used to map excel/csv file columns with the columns of database
# fetch_schema - is used to fetch datatypes of destination table, which is used while validating records and type conversion
# validate_record - is used for validation of records and converting data type of files w.r.t sql
########################################################################################################


logger = get_logger()
config = read_config()

def dump_file(file_ops_context):
    try:
        fileType = file_ops_context['FileType']
        
        if(fileType.lower() == "csv"):
            status, dataParse, (column_mapping, table_schema) = parse_csv_file(file_ops_context)
        elif(fileType.lower() == 'excel'):
            status, dataParse, (column_mapping, table_schema) = parse_excel_file(file_ops_context)
        if(status == False):
            return create_failure(500, 'Error while parsing uploaded files', 'Failed')
        status, dataUpdated = validate_record(file_ops_context, dataParse, column_mapping, table_schema)   
        if(status == False):
            return create_failure(500, 'Too many records with mismatched datatypes', 'Failed')
        response = insert_into_db(file_ops_context, dataUpdated, column_mapping)
        return response
    except Exception as e:
        logger.error("Error While Dumping File"+str(e))
        return create_failure(500, 'Error Wile Dumping File', 'Failed')
 

def parse_csv_file(file_ops_context):
    try:
        filePath = file_ops_context['FilePath']
    except Exception as e:
        logger.error("Error While Fetching File Path"+str(e))
        return create_failure(500, 'Error While Fetching File Path', 'Failed')
    try:
        textQual = file_ops_context['textqualifier']
        if(len(textQual) == 0):
            textQual = ' '
    except Exception as e:
        logger.error("Error While Fetching TextQualifier "+str(e))
        return create_failure(500, 'Error While Fetching TextQualifier', 'Failed')
    try:
        rowDel = base64.b64decode(file_ops_context['rowdelimeter']).decode("utf-8")
    except Exception as e:
        logger.error("Error While Fetching Row Delimeter"+str(e))
        return create_failure(500, 'Error While Fetching Row Delimeter', 'Failed')
    try:
        colDel = base64.b64decode(file_ops_context['columndelimeter']).decode("utf-8")
    except Exception as e:
        logger.error("Error While Fetching Column Delimeter"+str(e))
        return create_failure(500, 'Error While Fetching Column Delimeter', 'Failed')
    try:
        noRowToSkip = int(file_ops_context['numberRowToSkip'])
    except Exception as e:
        logger.error("Error While Fetching Number of Rows to Skip"+str(e))
        return create_failure(500, 'Error While Fetching Number of Rows to Skip', 'Failed')
    try:
        firstRowHeader = file_ops_context['iscolumnnameinfirstrow']
        if(firstRowHeader == 'true'):
            firstRowHeader = 0
        else:
            firstRowHeader = None
    except Exception as e:
        logger.error("Error While Fetching Is ColumnName in First Row "+str(e))
        return create_failure(500, 'Error While Fetching Is ColumnName in First Row ', 'Failed')
    try:
        with open(filePath) as csv_data:
            data = csv_data.read()
        if(len(textQual) != 1):
            data = data.replace(textQual,"'")
            textQual = "'"
    except Exception as e:
        logger.error("Error While Processing CSV File "+str(e))
        return create_failure(500, 'Error While Processing CSV File', 'Failed')
    try:

        if(rowDel == '/r/n' or rowDel == '//r//n' or rowDel == '\\r\\n' or rowDel == '\r\n'):
            data = pd.read_csv(io.StringIO(data),encoding='ISO-8859-1',sep=colDel,header=firstRowHeader,skiprows=noRowToSkip,quotechar=textQual)
        elif(rowDel == '/n'):
            data = pd.read_csv(io.StringIO(data),encoding='ISO-8859-1',sep=colDel,lineterminator='\n',header=firstRowHeader,skiprows=noRowToSkip,quotechar=textQual)
        elif(rowDel == '/r'):
            data = pd.read_csv(io.StringIO(data),encoding='ISO-8859-1',sep=colDel,lineterminator='\r',header=firstRowHeader,skiprows=noRowToSkip,quotechar=textQual)
        else:
            data = pd.read_csv(io.StringIO(data),encoding='ISO-8859-1',sep=colDel,lineterminator=rowDel,header=firstRowHeader,skiprows=noRowToSkip,quotechar=textQual)
        #data.replace({pd.NaT: None}, inplace=True)
        #data.replace({pd.NA: None}, inplace=True)
        tableMapping = table_column_mapping(file_ops_context)
        first_column = data.columns[0]
        if not first_column in tableMapping:
            for i in range(len(first_column)):
                if len(first_column) > i + 1 and first_column[i+1:] in tableMapping:
                    tableMapping[first_column] = tableMapping[first_column[i+1:]]
                    continue
        if(tableMapping is False):
            return create_failure(500, 'Error while Mapping File Columns to Table Columns', 'Failed')
        tableSchema = fetch_schema(file_ops_context,data)
        if(tableSchema is False):
            return create_failure(500, 'Error While Fetching Schema of table', 'Failed')
    except Exception as e:
        logger.error("Error While Processing CSV File "+str(e))
        return create_failure(500, 'Error While Processing CSV File', 'Failed')
    return (True, data, (tableMapping, tableSchema))

def parse_excel_file(file_ops_context):
    try:
        filePath = file_ops_context['FilePath']
        sheetName = file_ops_context['SheetName']
        if sheetName is None:
            return create_failure(500, 'Empty sheetname', 'Failed')        
    except Exception as e:
        logger.error("Error While Fetching Request Data"+str(e))
        return create_failure(500, 'Error While Fetching Request Data', 'Failed')
    try:
        data = pd.read_excel(filePath,sheet_name=sheetName, dtype='object')  
        #data.replace({pd.NaT: None}, inplace=True)
        #data.replace({pd.NA: None}, inplace=True)
        #data.replace({np.nan: None}, inplace=True)
        column_mapping = table_column_mapping(file_ops_context)
        if column_mapping == False:
            return create_failure(500, 'Error while Mapping File Columns to Table Columns', 'Failed')
        table_schema = fetch_schema(file_ops_context,data)
        if table_schema == False:
            return create_failure(500, 'Error While Fetching Schema of table', 'Failed')        
    except Exception as e:
        logger.error("Error While Processing Excel File "+str(e))
        return create_failure(500, 'Error While Processing Excel File', 'Failed')
    return (True, data, (column_mapping, table_schema))

def insert_into_db(file_ops_context, data, column_mapping):
    try:
        overwriteMerge = file_ops_context['Overwrite_merge']
        databaseName = file_ops_context['DatabaseName']
        tableName = file_ops_context['TableName']
    except Exception as e:
        logger.error("Error While Inserting into Database"+str(e))
        return create_failure(500, 'Error While Inserting into Database', 'Failed')
    try:
        connection = db_conn(databaseName)
    except Exception as e:
        logger.error("Error While Creation of Engine "+str(e))
        return create_failure(500, 'Error While Creation of Engine', 'Failed')
    try:
        try:
            if(overwriteMerge == "O"):
                cursor = connection.cursor()
                cursor.execute('TRUNCATE TABLE {}'.format(tableName))
                connection.commit()
                cursor.close()
            elif(overwriteMerge != "M"):
                return create_failure(500, 'Error with unknown db write options', 'Failed')
        except Exception as e:
            logger.error("Error While Inserting into DB "+str(e))
            return create_failure(500, 'Error While Inserting into DB', 'Failed')
        cursor = connection.cursor()
        mapped_sql_cols = [k for k,v in column_mapping.items() if not 'NA' in v]
        tuples = [tuple(x) for x in data[mapped_sql_cols].to_numpy()]
        sql_cols = [column_mapping[col] for col in data[mapped_sql_cols]]
        cols_str = "],[".join([str(i) for i in sql_cols])
        databaseChunk = int(config['UPLOAD']['database_chunk'])
        chunk = int(len(tuples)/databaseChunk)
        cursor.fast_executemany = True
        for i in range(1,chunk+2):
            try:
                val = tuples[(i-1)*databaseChunk:i*databaseChunk]
                if not val:
                    continue
                query = upload_file_sql_query(tableName, cols_str, len(data[mapped_sql_cols].columns))
                cursor.executemany(query,val)
            except Exception as e:
                cursor.close()
                connection.close()
                logger.error("Error While Inserting into DB "+str(e))
                return create_failure(500, 'Error While Inserting into DB', 'Failed')
        connection.commit()
        cursor.close()
        connection.close()
        logger.info("Success")
    except Exception as e:
        logger.error("Error While Inserting into DB "+str(e))
        return create_failure(500, 'Error While Inserting into DB', 'Failed')
    return create_success('Data Dumped Successfully','')

def table_column_mapping(file_ops_context):
    try:
        clientId = file_ops_context['ClientID']
        tableName = file_ops_context['TableName']
        tableMapping = dict()
        excelImportMappingMainObject = Excelimportmappingmain.objects.filter(customercode=clientId, tablename = tableName)
        excelImportMappingMainData = excelimportmappingmainSerializer(excelImportMappingMainObject, many=True)
        if excelImportMappingMainObject.exists():
            try:
                for excelImportMappingMainRecord in excelImportMappingMainData.data:
                    try:
                        pid = excelImportMappingMainRecord['excelimportmappingmainid']
                        excelImportMappingDetailObject = ExcelImportMappingDetail.objects.filter(pid=pid)
                        excelImportMappingDetailData= excelimportmappingdetailSerializer(excelImportMappingDetailObject, many=True)
                        if excelImportMappingDetailObject.exists():
                            for mappingRecord in excelImportMappingDetailData.data:
                                tableMapping[mappingRecord['excelcolumncaption']] = mappingRecord['sqlcolumncaption']                  
                    except Exception as e:
                        logger.error("Error in ExcelImportMappingDetail: "+str(e))
                        return False
            except Exception as e:
                logger.error("Error in Excelimportmappingmain Table: "+str(e))
                return False
    except Exception as e:
        logger.error("Error in Fetching Data."+str(e))    
        return False
    return tableMapping

def fetch_schema(file_ops_context,data):
    try:
        databaseName = file_ops_context['DatabaseName']
        tableName = file_ops_context['TableName']
    except Exception as e:
        logger.error("Error While Reteriving Payload"+str(e))
        return False
    try:
        connection = db_conn(databaseName)
        cursor = connection.cursor()
    except Exception as e:
        logger.error("Error While Creation of DB Connection "+str(e))
        return False
    try:
        query =  """SELECT sysColumn.name,sysColumn.max_length,sysType.name 
                  FROM sys.columns as sysColumn 
                  JOIN sys.types as sysType 
                  ON sysColumn.user_type_id = sysType.user_type_id 
                  WHERE sysColumn.object_id = Object_id('{}')""".format(tableName)
        queryResult = cursor.execute(query).fetchall()
        cursor.close()
        connection.close()
    except Exception as e:
        logger.error("Error While Performing Fetch Schema Operation"+str(e))
        return False
    return queryResult

def validate_record(file_ops_context, data, column_mapping, table_schema):
    try:
        column_datatype = dict()
        column_max_length = dict()
        for var in table_schema:
            column_datatype[var[0]] = var[2]
            column_max_length[var[0]] = var[1]

        totalColumns, totalRows = data.shape
        max_invalid_record_count = float(config['UPLOAD']['data_type_check_threshold']) * totalRows * totalColumns
        invalid_record_count = 0
        for column in data:
            if invalid_record_count > max_invalid_record_count:
                return False, None
            try:
                if not column in column_mapping:
                    continue
                mapped_column = column_mapping[column]
                mapped_dtype = column_datatype[mapped_column].lower()
                mapped_length = int(column_max_length[mapped_column])
            except Exception as e:
                logger.error("Error get mapping: "+str(e))
                return False, None
            try:
                if mapped_dtype in ('float', 'decimal', 'double'):
                    data[column] = data[column].astype('float64', errors='ignore')
                    invalid_record_count += data[column].apply(lambda x: type(x) != float).sum()
                elif mapped_dtype in ('int', 'integer'):
                    data[column] = data[column].astype('int64', errors='ignore')
                    invalid_record_count += data[column].apply(lambda x: type(x) != int).sum()
                elif mapped_dtype in ('date', 'datetime'):
                    data[column] = data[column].astype(str)
                    if mapped_dtype == 'date':
                        #data[column] = data[column].apply(lambda x: '2199-12-31' if '9999' in x else x)
                        data[column] = data[column].astype('datetime64', errors='ignore').astype(str)
                        data[column] = data[column].apply(lambda x: x.strip()[:10])
                    invalid_record_count += data[column].apply(lambda x: type(x) != str).sum()
                elif mapped_dtype in ('boolean', 'bool'):
                    data[column] = data[column].astype(int, errors='ignore')
                    invalid_record_count += data[column].apply(lambda x: type(x) != int).sum()
                elif mapped_dtype in ('varchar', 'nvarchar'):
                    data[column] = data[column].astype(str)
                    if  mapped_length > 0:
                        data[column] = data[column].apply(lambda x: x[:mapped_length])
                    invalid_record_count += data[column].apply(lambda x: type(x) != str).sum()
            except Exception as e:
                logger.error("Error While Validating Record"+str(e))
                return False, None
    except Exception as e:
        logger.error("Error While Preparing Validation Record"+str(e))
        return False, None
    return True, data
