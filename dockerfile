From python:3.8-slim

RUN apt-get update && apt-get install --fix-missing
RUN apt-get update && apt-get install -y curl apt-utils gcc g++ libc-dev unixodbc-dev

RUN apt-get update && apt-get install -y \
    curl apt-utils apt-transport-https debconf-utils gcc-7 build-essential g++-7\
    && rm -rf /var/lib/apt/lists/*

# adding custom MS repository
#RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -

RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/debian/10/prod.list > /etc/apt/sources.list.d/mssql-release.list

# install SQL Server drivers
#RUN apt-get update && ACCEPT_EULA=Y apt-get install -y msodbcsql unixodbc-dev 
RUN apt-get update && ACCEPT_EULA=Y apt-get install -y msodbcsql17

# install ms-tools
RUN ACCEPT_EULA=Y apt-get install -y mssql-tools
RUN echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile
RUN echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc
#RUN source ~/.bashrc

RUN apt-get install unixodbc-dev
RUN apt-get install libgssapi-krb5-2
 
COPY ./requirements.txt /app/requirements.txt
 
WORKDIR /app
 
RUN pip install --upgrade pip
 
RUN pip install -r requirements.txt
 
COPY . /app
 
ENTRYPOINT ["gunicorn"]
CMD [ "digicube.wsgi" ]
