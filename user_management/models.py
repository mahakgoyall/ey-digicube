from django.db import models

# Create your models here.
class CountryDetails(models.Model):
    countryname = models.CharField(db_column='CountryName', primary_key=True, max_length=50)  # Field name made lowercase.
    telephonecode = models.CharField(db_column='TelephoneCode', max_length=20, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'CountryDetails'

class Branchdetails(models.Model):
    id = models.CharField(db_column='ID', primary_key=True, max_length=50)  # Field name made lowercase.
    branchcode = models.CharField(db_column='BranchCode', max_length=20, blank=True, null=True)  # Field name made lowercase.
    branchname = models.CharField(db_column='BranchName', max_length=50, blank=True, null=True)  # Field name made lowercase.
    createdby = models.CharField(db_column='CreatedBy', max_length=100, blank=True, null=True)  # Field name made lowercase.
    createddate = models.DateTimeField(db_column='CreatedDate', blank=True, null=True)  # Field name made lowercase.
    modifiedby = models.CharField(db_column='ModifiedBy', max_length=100, blank=True, null=True)  # Field name made lowercase.
    modifieddate = models.DateTimeField(db_column='ModifiedDate', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'BranchDetails'