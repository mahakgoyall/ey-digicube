import sys 
sys.path.append('..')
from common_files.read_configuration import read_config
from common_files.create_connection import db_conn
from common_files.read_logger import get_logger
from sql_queries import cronjob_query
from datetime import date
import ldap3
import base64

config=read_config()
logger = get_logger()
current_date = date.today()
ad_user_error_map = {
                        2:"ACCOUNTDISABLE",
                        514:"Disabled",
                        546:"Password Not Required",
                        66050:"Disabled, Password Doesn’t Expire",
                        66082: "Disabled, Password Doesn’t Expire & Not Required",
                        262658: "Disabled, Smartcard Required",
                        262690:"Disabled, Smartcard Required,Password Not Required",
                        328194:"Disabled, Smartcard Required,Password Doesn’t Expire",
                        328226:"Disabled, Smartcard Required,Password Doesn’t Expire & Not Required"
                    }
try:
    connection = db_conn(commit=True)
    cursor = connection.cursor()
    server_name = base64.b64decode(str(config['LDAP']['server_name'])).decode("utf-8")
    user_name = base64.b64decode(str(config['LDAP']['user_name'])).decode("utf-8")
    password = base64.b64decode(str(config['LDAP']['password'])).decode("utf-8")
    server = ldap3.Server(server_name, get_info=ldap3.ALL)
    conn = ldap3.Connection(server, user=user_name, password=password,auto_bind=True)
    update_query=cronjob_query.update_user_status
    fetch_query = cronjob_query.fetch_all_users()
    try:
        response = cursor.execute(fetch_query)
        all_users=response.fetchall()
        for user in all_users:
            user_id_details=user[0].split("\\")
            if len(user_id_details) ==2:
                user_domain = user_id_details[0]
                user_id = user_id_details[1]
                print(user_domain,user_id)
                conn.search('dc={},dc=com'.format(user_domain), f'(&(objectclass=person)(sAMAccountName={user_id}))', attributes=[ldap3.ALL_ATTRIBUTES, ldap3.ALL_OPERATIONAL_ATTRIBUTES])
                try:
                    ad_user_details=conn.entries[0]
                except Exception as error:
                    logger.error(f"User: {user[0]} is absent in AD")
                    cursor.execute(update_query(user_id,0,"Absent"))
                try:
                    if (int(str(ad_user_details.userAccountControl)) in ad_user_error_map ) and (user[1] == "Active"):
                        print(f"Setting user - {user_id} as Terminated")
                        cursor.execute(update_query(user[0],0,"Terminated"))
                    elif (str(ad_user_details.userAccountControl) == "512" ) and (user[1] == "Terminated"):
                        print(f"Setting user - {user_id} as Active")
                        cursor.execute(update_query(user[0],1,"Active"))
                    elif (not (int(str(ad_user_details.userAccountControl)) in ad_user_error_map )) and  (not (str(ad_user_details.userAccountControl) == "512" )):
                        logger.error(f"Issue with user-{user[0]}, AD Status set as {ad_user_details.userAccountControl}")
                except Exception as error:
                    logger.error("Error while fetching details",exc_info=True)
            else:
                logger.error(f"Error with user: {user[0]}")
        cursor.close()
        connection.close() 
    except Exception as e:
        logger.error("Error During Creation of SQL Queries -"+str(e))
except Exception as e:
    logger.error("Error During DB Connection -"+str(e))