from django.urls import  path
from . import views

urlpatterns = [
    path('api/create-user', views.AddUser.as_view()),
    path('api/user-listing', views.user_listing_api.as_view()),
    path('api/approve-user', views.approve_user_new.as_view()),
    path('api/edit-user', views.edit_user.as_view()),
    path('api/delete-user', views.active_or_inactive_user.as_view()),
    path('api/user-log-details', views.user_log_details.as_view()),
    path('api/country-details', views.country_details.as_view()),
    path('api/branch-code-details', views.branch_code_details.as_view()),
]