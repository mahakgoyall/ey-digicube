from rest_framework import serializers
from login.models import Userlogindetails
from dashboard.models import Settings, Clientmaster, Sectormaster
from .models import CountryDetails, Branchdetails

class UserLoginSerializer(serializers.ModelSerializer):

    class Meta:
        model = Userlogindetails
        fields = '__all__'

class settingsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Settings
        fields = '__all__'

class clientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Clientmaster
        fields = '__all__'

class sectorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Sectormaster
        fields = '__all__'


class countryDetailsSerializer(serializers.ModelSerializer):

    class Meta:
        model = CountryDetails
        fields = '__all__'

class branchDetailsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Branchdetails
        fields = '__all__'