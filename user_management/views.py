from django.http.response import JsonResponse
from django.utils import timezone
from rest_framework.views import APIView
from rest_framework.request import Request
from common_files.read_logger import get_logger
from common_files.read_configuration import read_config
from common_files.create_response import create_failure, create_success
from common_files.create_connection import db_conn
from common_files.hash_password import create_hash_password

from dashboard.models import  Settings
from .models import CountryDetails, Branchdetails
from .serializers import settingsSerializer, branchDetailsSerializer

from sql_queries.logs_query import session_details, current_login_details, created_user_history, update_user_history, InActive_user_history
from login.models import Userlogindetails
from dashboard.models import Usertoclientmapping, Usertosectormapping, Usertosettingsmapping, Clientmaster, Sectormaster
from common_files.jwt_token import make_jwt_token, verify_jwt_token
import json
import requests
from rest_framework.request import HttpRequest, Request
import datetime
from .serializers import UserLoginSerializer, sectorSerializer, clientSerializer, countryDetailsSerializer
import pyodbc
import pandas as pd
import time, os, uuid
import hashlib
import hmac
import base64
import random,string
import ldap3
import sys
from ldap3 import Server, Connection, ALL, NTLM, ALL_ATTRIBUTES, ALL_OPERATIONAL_ATTRIBUTES, AUTO_BIND_NO_TLS, SUBTREE
from ldap3.core.exceptions import LDAPCursorError



class AddUser(APIView):

    def __init__(self):
        self.logger = get_logger()
        self.config = read_config()

    def post(self, request: Request) -> JsonResponse:
        try:
            userid = verify_jwt_token(request.META['HTTP_AUTHORIZATION'])
            if userid == -1:
                self.logger.error("Token is Expired")
                return JsonResponse(create_failure(401, 'Token is Expired', 'Fail'))
            elif userid == 0:
                self.logger.error("Error in add user API token: ")
                return JsonResponse(create_failure(400, 'Invalid token', 'Fail'))
        except Exception as e:
            self.logger.error("Error in add user API: "+str(e))
            return JsonResponse(create_failure(400, 'Please provide valid token', 'Fail'))
        try:
            self.user_id = userid
            self.id = request.data['ID']
            self.user_type = request.data['UserType']
            self.first_name = request.data['FirstName']
            self.last_name = request.data['LastName']
            self.contact_no = request.data['ContactNo']   
            self.email_address = request.data['EmailAddress']
            self.country = request.data['Country']
            self.city = request.data['City']
            self.state = request.data['State']
            self.branch_code = request.data['BranchCode']
        except Exception as e:
            self.logger.error("Error in Payload"+str(e))
            return JsonResponse(create_failure(500, 'Erorr in Payload', 'Fail'))

        try:
            # here using isactive__in for just checking if initial created user or active user exist with given mail id
            try:
                server_name = base64.b64decode(str(self.config['LDAP']['server_name'])).decode("utf-8")
                domain_name = (self.id).split("\\")[0].lower()
                employee_id = (self.id).split("\\")[1].lower()
                user_name = base64.b64decode(str(self.config['LDAP']['user_name'])).decode("utf-8")
                password = base64.b64decode(str(self.config['LDAP']['password'])).decode("utf-8")
                server = Server(server_name, get_info=ALL)
                conn = Connection(server, user=user_name, password=password,auto_bind=True)
                self.logger.info("LDAP Connection Successfully established")
                # conn.search('dc={},dc=com'.format(domain_name), '(objectclass=person)', attributes=[ALL_ATTRIBUTES, ALL_OPERATIONAL_ATTRIBUTES])
                query_set_ad = f'(&(objectclass=person)(|(userPrincipalName={self.email_address})(mail={self.email_address}))(sAMAccountName={employee_id}))'
                response = conn.search('dc={},dc=com'.format(domain_name), query_set_ad , attributes=[ldap3.ALL_ATTRIBUTES, ldap3.ALL_OPERATIONAL_ATTRIBUTES])
                if response:
                    if len(conn.entries) == 1:
                        try:
                            user_account_control=conn.entries[0].userAccountControl
                            if user_account_control in [512,2]:
                                self.logger.info("User is active/locked")
                            else:
                                self.logger.error("User is not authorized")
                                return JsonResponse(create_failure(400, 'User is not in active/lock state!', 'Fail'))
                        except Exception as e:
                            self.logger.error("Cannot find userAccountControl in conn.entries object")
                            return JsonResponse(create_failure(500, 'Internal Error!', 'Fail'))
                        self.logger.info("Correct User Details!")
                    else:
                        self.logger.error("Details of user are incorrect!")
                        return JsonResponse(create_failure(500, 'Details of user are incorrect!', 'Fail'))
                else:
                    self.logger.error("Error with AD-DS search!")
                    return JsonResponse(create_failure(500, 'Internal Error!', 'Fail'))
                                            
            except Exception as e:
                self.logger.error("Error During Connection with AD"+str(e))
                return JsonResponse(create_failure(500, 'Error During Connection with AD: Please make sure domain is correct!', 'Fail'))


            user_count_object = Userlogindetails.objects.filter(id = self.id).count()
            if(user_count_object != 0 ):
                self.logger.error("Employee ID Already Exist")
                return JsonResponse(create_failure(500, 'Employee ID Already Exist', 'Fail'))
            user_count_object = Userlogindetails.objects.filter(emailaddress = self.email_address).count()
            if(user_count_object != 0 ):
                self.logger.error("Email Address Already Exist")
                return JsonResponse(create_failure(500, 'EmailAddress Already Exist', 'Fail'))
        except Exception as e:
            self.logger.error("User Already Exist:-"+str(e))
            return JsonResponse(create_failure(500, 'User Already Exist', 'Fail'))            
        try:
            self.status = "Inactive"
            self.is_approved = 0
            self.password = create_hash_password(self.get_random_string(8))
            user_add = Userlogindetails.objects.create(id=self.id, password=self.password, usertype=self.user_type ,name=self.first_name, lastname=self.last_name, mobileno=self.contact_no, emailaddress=self.email_address, reasonofchange="New User Added", createdby=self.user_id, createddate=timezone.now(), modifiedby=self.user_id, modifieddate=timezone.now(), status=self.status, isapproved=self.is_approved, country=self.country, city=self.city, state=self.state, branchcode=self.branch_code, isActive=0)
            if not user_add:
                self.logger.error("Failed To Add User: ")
                return JsonResponse(create_failure(500, 'Failed To Add User', 'Fail'))
            
            SectorList = Sectormaster.objects.filter().values_list('sectormasterid', flat=True)
            if(SectorList.exists()):
                for sector_id in SectorList:
                    try:
                        sector_unique_id = uuid.uuid4()
                        user_sector_add = Usertosectormapping.objects.create(usertosectorid=sector_unique_id, userid=self.id, sectorid = sector_id, createdby = self.user_id, createddate=timezone.now(), modifiedby=self.user_id, modifieddate=timezone.now())
                        if not user_sector_add:
                            self.logger.error("Failed To Add User Sector Mapping: ")
                            return JsonResponse(create_failure(500, 'Failed To Add User Sector Mapping', 'Fail'))
                    except Exception as e:
                            self.logger.error("Failed To Add User Sector Mapping: "+str(e))
                            return JsonResponse(create_failure(500, 'Failed To Add User Sector Mapping', 'Fail'))
                        

            ClientList = Clientmaster.objects.filter().values_list('clientmasterid', flat=True)
            if(ClientList.exists()):
                for client_id in ClientList:
                    try:
                        client_unique_id = uuid.uuid4()
                        user_client_add = Usertoclientmapping.objects.create(userclientmappingid=client_unique_id, userid=self.id, clientid = client_id, createdby = self.user_id, createddate=timezone.now(), modifiedby=self.user_id, modifieddate=timezone.now())
                        if not user_client_add:
                            self.logger.error("Failed To Add User Client Mapping: ")
                            return JsonResponse(create_failure(500, 'Failed To Add User Client Mapping', 'Fail'))
                    except Exception as e:
                            self.logger.error("Failed To Add User Client Mapping: "+str(e))
                            return JsonResponse(create_failure(500, 'Failed To Add User Client Mapping', 'Fail'))

            settings_unique_id = uuid.uuid4()
            user_settings_add = Usertosettingsmapping.objects.create(usertosettingsid=settings_unique_id, userid=self.id, settingsid = 1, createdby = self.user_id, createddate=timezone.now(), modifiedby=self.user_id, modifieddate=timezone.now())
            if not user_settings_add:
                self.logger.error("Failed To Add User Settings Mapping: ")
                return JsonResponse(create_failure(500, 'Failed To Add User Settings Mapping', 'Fail'))

            # client_unique_id = uuid.uuid4()
            # user_add = Userlogindetails.objects.create(id=self.id, password=self.password, usertype=self.user_type ,name=self.first_name, lastname=self.last_name, mobileno=self.contact_no, emailaddress=self.email_address, reasonofchange="New User Added", createdby=self.user_id, createddate=timezone.now(), modifiedby=self.user_id, modifieddate=timezone.now(), status=self.status, isapproved=self.is_approved, country=self.country, city=self.city, state=self.state, branchcode=self.branch_code, isActive=0)

            
        except Exception as e:
            self.logger.error("Failed To Add User: "+str(e))
            return JsonResponse(create_failure(500, 'Failed To Add User', 'Fail'))
        return JsonResponse(create_success('User Added Successfully', []))

 
    def get_random_string(self,length):
        letters = string.ascii_lowercase
        password = ''.join(random.choice(letters) for i in range(length))
        return password


class user_listing_api(APIView):
    def __init__(self):
        self.logger = get_logger()
    
    def get(self, request: Request) -> JsonResponse:
        return JsonResponse({"Message": "Method Get Is Not Allowed While APi is working"})
    
    def post(self, request: Request) -> JsonResponse:
        try:
            userid = verify_jwt_token(request.META['HTTP_AUTHORIZATION'])
            if userid == -1:
                self.logger.error("Token is Expired")
                return JsonResponse(create_failure(401, 'Token is Expired', 'Fail'))
            elif userid == 0:
                self.logger.error("Error in user list API token: ")
                return JsonResponse(create_failure(400, 'Invalid token', 'Fail'))
        except Exception as e:
            self.logger.error("Error in user list API: "+str(e))
            return JsonResponse(create_failure(400, 'Please provide valid token', 'Fail'))

        user_role_type = Userlogindetails.objects.get(id=userid).usertype
        print(user_role_type)
        self.logger.info(userid)
        try:
            if user_role_type == "AdminMaker":
                user_list_object = Userlogindetails.objects.filter(createdby=userid).values_list('id','usertype', 'name', 'lastname', 'country', 'city', 'state', 'mobileno', 'emailaddress', 'createdby', 'createddate', 'modifiedby', 'modifieddate', 'isapproved', 'approvedby', 'approvedon', 'gender', 'salutation', 'branchcode', 'status', 'reasonofchange','isActive').order_by('-modifieddate')
            elif user_role_type == "AdminChecker":    
                ####### Removing Inactive from status__in
                user_list_object = Userlogindetails.objects.filter(isapproved=0).values_list('id','usertype', 'name', 'lastname', 'country', 'city', 'state', 'mobileno', 'emailaddress', 'createdby', 'createddate', 'modifiedby', 'modifieddate', 'isapproved', 'approvedby', 'approvedon', 'gender', 'salutation', 'branchcode', 'status', 'reasonofchange','isActive').order_by('-modifieddate')
            
            user_list = []
            try:
                for user in user_list_object:
                    user_dict = {}
                    user_dict['ID'] = user[0]
                    user_dict['UserType'] = user[1]
                    user_dict['FirstName'] = user[2]
                    user_dict['LastName'] = user[3]
                    user_dict['Country'] = user[4]
                    user_dict['City'] = user[5]
                    user_dict['State'] = user[6]
                    user_dict['MobileNo'] = user[7]
                    user_dict['EmailAddress'] = user[8]
                    user_dict['CreatedBy'] = user[9]
                    user_dict['CreatedDate'] = user[10]
                    user_dict['ModifiedBy'] = user[11]
                    user_dict['ModifiedDate'] = user[12]
                    user_dict['IsApproved'] = user[13]
                    user_dict['ApprovedBy'] = user[14]
                    user_dict['ApprovedOn'] = user[15]
                    user_dict['Gender'] = user[16]
                    user_dict['Salutation'] = user[17]
                    user_dict['BranchCode'] = user[18]
                    user_dict['Status'] = user[19]
                    user_dict['ReasonOfChange'] = user[20]
                    user_dict['IsActive'] = user[21]
                    user_list.append(user_dict)
            except Exception as e:
                self.logger.error('Error in fetching the data')
                return JsonResponse(create_failure(500, 'Error in fetching the data', 'Fail'))
        except Exception as e:
            self.logger.error("Error in User Login Table "+str(e))
            return JsonResponse(create_failure(500,'Error in UserListing API','Fail'))
        return JsonResponse(create_success('Success', user_list))


class approve_user_new(APIView):
    def __init__(self):
        self.logger = get_logger()
    
    def get(self, request: Request) -> JsonResponse:
        return JsonResponse({"Message": "Method Get Is Not Allowed While APi is working"})
    
    def post(self, request: Request) -> JsonResponse:
        try:
            userid = verify_jwt_token(request.META['HTTP_AUTHORIZATION'])
            if userid == -1:
                self.logger.error("Token is Expired")
                return JsonResponse(create_failure(401, 'Token is Expired', 'Fail'))
            elif userid == 0:
                self.logger.error("Error in approve user API token: ")
                return JsonResponse(create_failure(400, 'Invalid token', 'Fail'))
        except Exception as e:
            self.logger.error("Error in approve user API: "+str(e))
            return JsonResponse(create_failure(400, 'Please provide valid token', 'Fail'))

        try:
            self.id = request.data['EmployeeCode']
            self.requested_for = request.data['Requested']
            self.approve_or_reject = request.data['Approval']
            self.reason_of_change = request.data['Reason']
        except Exception as e:
            self.logger.error("Error in approve_user API: "+str(e))
            return JsonResponse(create_failure(500, 'Error In Payload', 'Fail'))
        try:
            if self.approve_or_reject == "Approve":
                if self.requested_for == 'Inactive':
                    user_approve = Userlogindetails.objects.filter(id=self.id).update(status="Inactive",approvedby=userid, isapproved=1, approvedon=timezone.now(), reasonofchange=self.reason_of_change)
                    response_message_1 = "User Inactive Success"
                    response_message_2 = "User Already Inactive"

                elif self.requested_for == 'Active':
                    user_approve = Userlogindetails.objects.filter(id=self.id).update(status="Active",approvedby=userid, isapproved=1, approvedon=timezone.now(), reasonofchange=self.reason_of_change, isActive=1)
                    response_message_1 = "User Active Success"
                    response_message_2 = "User Already Active"
    
            elif self.approve_or_reject == "Reject":
                if self.requested_for == 'Inactive':
                    user_approve = Userlogindetails.objects.filter(id=self.id).update(status="Active",approvedby=userid, isapproved=1, approvedon=timezone.now(), reasonofchange=self.reason_of_change)
                    response_message_1 = "User Inactive Reject Sucess"
                    response_message_2 = "User Already Inactive Rejected"

                elif self.requested_for == 'Active':
                    requested = Userlogindetails.objects.filter(id=self.id, isActive = 1)
                    if(requested.exists()):
                        user_approve = Userlogindetails.objects.filter(id=self.id).update(status="Inactive",approvedby=userid, isapproved=1, approvedon=timezone.now(), reasonofchange=self.reason_of_change)
                        response_message_1 = "User Active Reject Success"
                        response_message_2 = "User Already Active Rejected"
                    else:
                        user_approve = Userlogindetails.objects.filter(id=self.id).update(status="Reject",approvedby=userid, isapproved=1, approvedon=timezone.now(), reasonofchange=self.reason_of_change)
                        response_message_1 = "User Reject Sucess"
                        response_message_2 = "User Already Reject Rejected"


            if user_approve == 0:
                self.logger.error("User Already Approved")
                return JsonResponse(create_failure(400, response_message_2, 'Fail'))

        except Exception as e:
            self.logger.error("Error in Error In Approve User: "+str(e))
            return JsonResponse(create_failure(500, 'Error In Approve User', 'Fail'))
        return JsonResponse(create_success(response_message_1,[]))




class approve_user(APIView):
    def __init__(self):
        self.logger = get_logger()
    
    def get(self, request: Request) -> JsonResponse:
        return JsonResponse({"Message": "Method Get Is Not Allowed While APi is working"})
    
    def post(self, request: Request) -> JsonResponse:
        try:
            userid = verify_jwt_token(request.META['HTTP_AUTHORIZATION'])
            if userid == -1:
                self.logger.error("Token is Expired")
                return JsonResponse(create_failure(401, 'Token is Expired', 'Fail'))
            elif userid == 0:
                self.logger.error("Error in approve user API token: ")
                return JsonResponse(create_failure(400, 'Invalid token', 'Fail'))
        except Exception as e:
            self.logger.error("Error in approve user API: "+str(e))
            return JsonResponse(create_failure(400, 'Please provide valid token', 'Fail'))

        try:
            self.id = request.data['EmployeeCode']
            self.requested_for = request.data['Requested']
            self.approve_or_reject = request.data['Approval']
            self.reason_of_change = request.data['Reason']
        except Exception as e:
            self.logger.error("Error in approve_user API: "+str(e))
            return JsonResponse(create_failure(500, 'Error In Payload', 'Fail'))
        try:
            if self.approve_or_reject == "Approve":
                if self.requested_for == 'Inactive':
                    user_approve = Userlogindetails.objects.filter(id=self.id, isapproved=1).update(status="Inactive",approvedby=userid, isapproved=0, approvedon=timezone.now(), reasonofchange=self.reason_of_change, isActive=0)
                    response_message_1 = "User Inactive Success"
                    response_message_2 = "User Already Inactive"

                elif self.requested_for == 'Active':
                    user_approve = Userlogindetails.objects.filter(id=self.id, isapproved=0).update(status="Active",approvedby=userid, isapproved=1, approvedon=timezone.now(), reasonofchange=self.reason_of_change, isActive=1)
                    response_message_1 = "User Active Success"
                    response_message_2 = "User Already Active"

                elif self.requested_for == 'UserApproval':
                    user_approve = Userlogindetails.objects.filter(id=self.id, isapproved=0).update(status="Active",approvedby=userid, isapproved=1, approvedon=timezone.now(), reasonofchange=self.reason_of_change, isActive=1)
                    response_message_1 = "User Approve Success"
                    response_message_2 = "User Already Approved"
    
            elif self.approve_or_reject == "Reject":
                if self.requested_for == 'Inactive':
                    user_approve = Userlogindetails.objects.filter(id=self.id, isapproved=1).update(status="Active",approvedby=userid, isapproved=1, approvedon=timezone.now(), reasonofchange=self.reason_of_change, isActive=1)
                    response_message_1 = "User Inactive Reject Sucess"
                    response_message_2 = "User Already Inactive Rejected"

                elif self.requested_for == 'Active':
                    user_approve = Userlogindetails.objects.filter(id=self.id, isapproved=0).update(status="Inactive",approvedby=userid, isapproved=0, approvedon=timezone.now(), reasonofchange=self.reason_of_change, isActive=0)
                    response_message_1 = "User Active Reject Success"
                    response_message_2 = "User Already Active Rejected"

                elif self.requested_for == 'UserApproval':
                    user_approve = Userlogindetails.objects.filter(id=self.id, isapproved=0).update(status="Reject",approvedby=userid, isapproved=0, approvedon=timezone.now(), reasonofchange=self.reason_of_change, isActive=0)
                    response_message_1 = "User Reject Sucess"
                    response_message_2 = "User Already Reject Rejected"

            if user_approve == 0:
                self.logger.error("User Already Approved")
                return JsonResponse(create_failure(400, response_message_2, 'Fail'))

        except Exception as e:
            self.logger.error("Error in Error In Approve User: "+str(e))
            return JsonResponse(create_failure(500, 'Error In Approve User', 'Fail'))
        return JsonResponse(create_success(response_message_1,[]))




class edit_user(APIView):
    def __init__(self):
        self.logger = get_logger()
    
    def get(self, request: Request) -> JsonResponse:
        return JsonResponse({"Message": "Method Get Is Not Allowed While APi is working"})
    
    def post(self, request: Request) -> JsonResponse:
        try:
            userid = verify_jwt_token(request.META['HTTP_AUTHORIZATION'])
            if userid == -1:
                self.logger.error("Token is Expired")
                return JsonResponse(create_failure(401, 'Token is Expired', 'Fail'))
            elif userid == 0:
                self.logger.error("Error in edit user API token: ")
                return JsonResponse(create_failure(400, 'Invalid token', 'Fail'))
        except Exception as e:
            self.logger.error("Error in edit user API: "+str(e))
            return JsonResponse(create_failure(400, 'Please provide valid token', 'Fail'))
        
        try:
            self.user_id = userid
            self.id = request.data['ID']
            self.first_name = request.data['FirstName']
            self.last_name = request.data['LastName']
            self.contact_no = request.data['ContactNo']   
            self.email_address = request.data['EmailAddress']
            self.country = request.data['Country']
            self.city = request.data['City']
            self.state = request.data['State']
            self.branch_code = request.data['BranchCode']
            self.reason_of_change = request.data['Reason']
        except Exception as e:
            self.logger.error("Error in Payload"+str(e))
            return JsonResponse(create_failure(500, 'Erorr in Payload', 'Fail'))
        
        try:
            update_user = Userlogindetails.objects.filter(id=self.id).update(name=self.first_name, lastname=self.last_name, mobileno=self.contact_no, emailaddress=self.email_address, reasonofchange=self.reason_of_change, modifiedby=self.user_id, modifieddate=timezone.now(), country=self.country, city=self.city, state=self.state, branchcode=self.branch_code)
            if update_user == 0:
                self.logger.error("Error Update User API"+str(e))
                return JsonResponse(create_failure(500, 'User Edit Api', 'Fail'))
        except Exception as e:
            self.logger.error("Error in Payload"+str(e))
            return JsonResponse(create_failure(500, 'Erorr in User Edit Api', 'Fail'))
        return JsonResponse(create_success('User Edited',[]))


class active_or_inactive_user(APIView):
    def __init__(self):
        self.logger = get_logger()
    
    def get(self, request: Request) -> JsonResponse:
        return JsonResponse({"Message": "Method Get Is Not Allowed While APi is working"})
    
    def post(self, request: Request) -> JsonResponse:
        try:
            userid = verify_jwt_token(request.META['HTTP_AUTHORIZATION'])
            if userid == -1:
                self.logger.error("Token is Expired")
                return JsonResponse(create_failure(401, 'Token is Expired', 'Fail'))
            elif userid == 0:
                self.logger.error("Error in active_inactive API token: ")
                return JsonResponse(create_failure(400, 'Invalid token', 'Fail'))
        except Exception as e:
            self.logger.error("Error in active_inactive API: "+str(e))
            return JsonResponse(create_failure(400, 'Please provide valid token', 'Fail'))
        
        try:
            self.id = request.data['EmployeeCode']
            self.deactive_or_active = request.data['IsActive']
            self.reason_of_change = request.data['Reason']
        except Exception as e:
            self.logger.error("Error in Payload : "+str(e))
            return JsonResponse(create_failure(400, 'Error in Payload: '+str(e), 'Fail'))
        
        try:
            user_inactive = Userlogindetails.objects.filter(id=self.id).update(isapproved=0)
            if user_inactive == 0:
                self.logger.error("Error in Updating"+str(e))
                return JsonResponse(create_failure(400, 'Error in Inactive user:', 'Fail'))
            if(self.deactive_or_active == 1):
                self.message = 'User Activated'
            else:
                self.message = 'User Deactivated'
        except Exception as e:
            self.logger.error("Error in Updating"+str(e))
            return JsonResponse(create_failure(400, 'Error in Inactive user: '+str(e), 'Fail'))
        return JsonResponse(create_success(self.message,[]))


class user_log_details(APIView):

    def __init__(self):
        self.logger = get_logger()
        self.config = read_config()
 
    def post(self, request: Request) -> JsonResponse:
        try:
            userid = verify_jwt_token(request.META['HTTP_AUTHORIZATION'])
            if userid == -1:
                self.logger.error("Token is Expired")
                return JsonResponse(create_failure(401, 'Token is Expired', 'Fail'))
            elif userid == 0:
                self.logger.error("Error in user details API token: ")
                return JsonResponse(create_failure(400, 'Invalid token', 'Fail'))
        except Exception as e:
            self.logger.error("Error in user details API: "+str(e))
            return JsonResponse(create_failure(400, 'Please provide valid token', 'Fail'))
         
        try:
            try:
                self.operation = request.data['operation']
                self.from_date = request.data['from_date']
                self.to_date = self.get_to_date(request.data['to_date'])
            except Exception as e:
                self.logger.error("Error in payload : "+str(e))
                return JsonResponse(create_failure(500, 'Error in Payload', 'Fail')) 
            
            try:
                settingsObject = Settings.objects.all()
                settingsData = settingsSerializer(settingsObject, many=True) 
                if(settingsData.data):
                    try:
                        self.appUrl = settingsData.data[0]['appurl']
                    except Exception as e:
                        self.logger.error("Error in Settings Table "+str(e))
                        return create_failure(500, 'Error in Settings Table', 'Failed')
                else:
                    self.logger.error("Error in Settings Table "+str(e))
                    return create_failure(500, 'Error in Settings Table', 'Failed')

                connection = db_conn()
                if(self.operation == 'Create'):
                    script = created_user_history(self.from_date,self.to_date)
                    self.unique_id='Created_User_History('+str(datetime.datetime.utcnow().strftime('%d-%m-%Y %H:%M:%S').replace(" ", "").replace("-", "").replace(":", "").replace(".", ""))+').xlsx'
                    sheet_name='Created_User_History'
                elif(self.operation == 'Update'):
                    script = update_user_history(self.from_date,self.to_date)
                    self.unique_id='Modified_User_History('+str(datetime.datetime.utcnow().strftime('%d-%m-%Y %H:%M:%S').replace(" ", "").replace("-", "").replace(":", "").replace(".", ""))+').xlsx'
                    sheet_name='Modified_User_History'
                elif(self.operation == 'InActive'):
                    script = InActive_user_history(self.from_date,self.to_date)
                    self.unique_id='Deleted_User_History('+str(datetime.datetime.utcnow().strftime('%d-%m-%Y %H:%M:%S').replace(" ", "").replace("-", "").replace(":", "").replace(".", ""))+').xlsx'
                    sheet_name='Deleted_User_History'
                else:
                    self.logger.error("Invalid Operation ")
                    return create_failure(500, 'Invalid Operation', 'Failed')
                self.logger.info(script)
                self.file_path = self.config['LOGS']['path']
                self.file_name = os.path.join(self.file_path, self.unique_id)
                self.logger.info(self.file_name)
                df = pd.read_sql_query(script, connection)
                if not os.path.exists(self.file_path):
                    os.makedirs(self.file_path)
                writer = pd.ExcelWriter(self.file_name)
                df.to_excel(writer, sheet_name, index=False)
                writer.save()
                download_url = self.appUrl + 'LogFiles/' + self.unique_id
                output = create_success('Log File URL', download_url)

            except Exception as e:
                self.logger.error("Error in user log details API: "+str(e))
                return JsonResponse(create_failure(500, 'user log details API Fail', 'Fail'))
            return JsonResponse(output)

        except Exception as e:
            self.logger.error("Error in user log details API: "+str(e))
            return JsonResponse(create_failure(400, 'user log details API Fail', 'Fail'))
        return JsonResponse(output)

    def get_to_date(self,to_date):
        if len(to_date) == 0:
            to_date = (timezone.now()).strftime("%Y-%m-%d")
        date_1 = datetime.datetime.strptime(to_date, "%Y-%m-%d")
        to_date = date_1 + datetime.timedelta(days=1)
        return to_date


class country_details(APIView):
    def __init__(self):
        self.logger = get_logger()
        self.config = read_config()
 
    def post(self, request: Request) -> JsonResponse:
        try:
            userid = verify_jwt_token(request.META['HTTP_AUTHORIZATION'])
            if userid == -1:
                self.logger.error("Token is Expired")
                return JsonResponse(create_failure(401, 'Token is Expired', 'Fail'))
            elif userid == 0:
                self.logger.error("Error in Contry-Details API token: ")
                return JsonResponse(create_failure(400, 'Invalid token', 'Fail'))
        except Exception as e:
            self.logger.error("Error in Contry-Details API: "+str(e))
            return JsonResponse(create_failure(400, 'Please provide valid token', 'Fail'))
        try:
            country_details = CountryDetails.objects.all()
            country_details = countryDetailsSerializer(country_details, many=True).data
        except Exception as e:
            self.logger.error("Error in Contry-Details API: "+str(e))
            return JsonResponse(create_failure(400, 'Error in Contry-Details API', 'Fail'))
        return JsonResponse(create_success('Success', country_details))

class branch_code_details(APIView):
    def __init__(self):
        self.logger = get_logger()
        self.config = read_config()
 
    def post(self, request: Request) -> JsonResponse:
        try:
            userid = verify_jwt_token(request.META['HTTP_AUTHORIZATION'])
            if userid == -1:
                self.logger.error("Token is Expired")
                return JsonResponse(create_failure(401, 'Token is Expired', 'Fail'))
            elif userid == 0:
                self.logger.error("Error in Branch-Code-Details API token: ")
                return JsonResponse(create_failure(400, 'Invalid token', 'Fail'))
        except Exception as e:
            self.logger.error("Error in Branch-Code-Details API: "+str(e))
            return JsonResponse(create_failure(400, 'Please provide valid token', 'Fail'))
        try:
            branch_details = Branchdetails.objects.all()
            branch_details = branchDetailsSerializer(branch_details, many=True).data
        except Exception as e:
            self.logger.error("Error in Branch-Code-Details API: "+str(e))
            return JsonResponse(create_failure(400, 'Error in Branch-Code-Details API', 'Fail'))
        return JsonResponse(create_success('Success', branch_details))