import sys 
sys.path.append('..')

from common_files.create_connection import db_conn
from common_files.read_logger import get_logger
from sql_queries import cronjob_query
from datetime import date

logger = get_logger()
current_date = date.today()
try:
    connection = db_conn(commit=True)
    cursor = connection.cursor()
    try:
        fetch_query = cronjob_query.fetch_users()
        response = cursor.execute(fetch_query)
        total_differnce_query = cronjob_query.user_settings()
        response_days = cursor.execute(total_differnce_query).fetchall()
        total_differnce_days = response_days[0][0]
        print(total_differnce_days)
        for user in response.fetchall() :
            try:
                # print(user)
                session_query = cronjob_query.fetch_sessions(user[0])
                # print(session_query)
                session_details = cursor.execute(session_query)
                for user_session in session_details.fetchall():
                    try:
                        total_differnce = current_date-user_session[0]
                        if(total_differnce.days >= total_differnce_days):
                            print(user[0])
                            update_query = cronjob_query.update_logindetails(user[0])
                            print(update_query)
                            cursor.execute(update_query)
                    except Exception as e:
                        logger.error("Error During Updation of table -"+str(e))
            except Exception as e:
                logger.error("Error During Creation of Session Queries -"+str(e))
        cursor.close()
        connection.close() 
    except Exception as e:
        logger.error("Error During Creation of SQL Queries -"+str(e))
except Exception as e:
    logger.error("Error During DB Connection -"+str(e))