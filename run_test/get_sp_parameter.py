from django.core.mail import message
from django.http.response import JsonResponse
from rest_framework.views import APIView
from rest_framework.request import Request
from rest_framework.request import HttpRequest, Request

from run_test import models
from dashboard.models import Ruledetails, Settings, Testparameter, Testparameterdetail
from event_emitter.models import Todolist

from common_files.read_logger import get_logger
from common_files.read_configuration import read_config
from common_files.create_connection import db_conn
from common_files.create_response import create_failure, create_success, create_success_modified

from upload_file.views import FetchDatabaseName

import requests, datetime, json, zipfile, zlib, xlsxwriter, time, csv, os
import digicube.settings as sts


def get_sp_payload(ruleDetailsId):
    logger = get_logger()
    try:
        payload = {}
        sptable = models.Storedproceduretable.objects.filter(ruledetailsid=ruleDetailsId).values_list('paramname',
                     'paramvalue')
        for sp in sptable:
            if not sp or not sp[1]:
                continue
            payload[sp[0]] = sp[1]

        tmtable = Testparameter.objects.filter(ruledetailsid=ruleDetailsId).values_list('testparameterid',
                    'caption', 'defaultvalue')
        if tmtable.exists():
            for tmitems in tmtable:
                try:
                    param_name = '_'.join(tmitems[1].split())
                    payload[param_name] = tmitems[2]
                    tmdetails = Testparameterdetail.objects.filter(ruledetailsid=ruleDetailsId, 
                        parameterid=tmitems[0]).values_list('value')
                    param_value = ''
                    for val in tmdetails:
                        for i in val:
                            param_value = i
                    # Replace default if value exists
                    if len(param_value) > 0:
                        payload[param_name] = param_value
                except Exception as e:
                    logger.error("Error in parsing ruledetails table: "+str(e))
                    return False
        logger.info("Payload for sp API: "+ str(payload))
    except Exception as e:
        logger.error("Error in preparing payload for sp_query: ", str(e))
        return False
    return payload

def sp_runtest(rule_id, ruleDetailsId, session_id) -> JsonResponse:
    logger = get_logger()
    try:
        dbVar = FetchDatabaseName()
        dbName = dbVar.fetch(session_id)
        connection = db_conn(dbName)
        payload = get_sp_payload(ruleDetailsId)
        if not payload:
             logger.info(f"No parameters for the stored procedure for {ruleDetailsId}!")
        sptable = models.Storedproceduretable.objects.filter(ruledetailsid=ruleDetailsId).values_list('storedprocname')
        cursor = connection.cursor()
        if not sptable:
            return create_failure(500, 'Error in fetching any valid or mapped stored procedures', 'Failed')
        for sp in sptable:
            try:
                sp_name = str(sp[0])
                param_str = ', '.join(['@' + key + '=?' for key in payload])
                query = ' '.join([f'exec {sp_name}', param_str])
                params = tuple(payload[k] for k in payload)
                logger.info(f'query: {query} and params: {",".join([str(v) for v in params])}')
                resultset = None
                if params:
                    resultset = cursor.execute(query, params).fetchall()
                else:
                    resultset = cursor.execute(query).fetchall()
                if not resultset[0][0]:
                    return create_failure(500, 'Error in executing stored procedure: getting empty results', 'Failed')
            except Exception as e:
                logger.error("Error in executing stored procedure: ", str(e))
                return create_failure(500, 'Error in executing stored procedure: ' + str(e), 'Failed')
        connection.commit()
        connection.close()
    except Exception as e:
        logger.error("Error in executing stored procedure: ", str(e))
        return create_failure(500, 'Error in executing stored procedure: ' + str(e), 'Failed')
    return create_success('Success')

def get_api_parameter(ruleDetailsId):
    logger = get_logger()
    try:
        payload = {}
        pythonfunc = models.Pythonfunctionstable.objects.filter(ruledetailsid=ruleDetailsId).values_list('paramname',
                    'paramvalue')
        if pythonfunc.exists():
            for key,value in pythonfunc:
                if not key:
                    continue
                payload[key] = value
        tmtable = Testparameter.objects.filter(ruledetailsid=ruleDetailsId).values_list('testparameterid',
                    'parametername', 'defaultvalue')
        if tmtable.exists():
            for tmitems in tmtable:
                try:
                    payload[tmitems[1]] = tmitems[2]
                    tmdetails = Testparameterdetail.objects.filter(ruledetailsid=ruleDetailsId, 
                        parameterid=tmitems[0]).values_list('value')
                    paramvalue = ''
                    for val in tmdetails:
                        for i in val:
                            paramvalue = i
                    # Replace default if value exists
                    if len(paramvalue) > 0:
                        payload[tmitems[1]] = paramvalue
                except Exception as e:
                    logger.error("Error in parsing ruledetails table: "+str(e))
                    return False
        if len(payload) == 0:
            return False
        logger.info("payload for API: "+ str(payload))
    except Exception as e:
        logger.error("Error in preparing payload for run test API: ", str(e))
        return False
    return payload

def api_run_test(rule_id, ruleDetailsId, session_id) -> JsonResponse:
    logger = get_logger()
    try:
        payload = get_api_parameter(ruleDetailsId)
        if not payload:
            logger.info("No parameters for the test api for {ruleDetailsID}")
            #return create_failure(500, 'Error in run test API', 'Failed')
        api_endpoint = ''
    except Exception as e:
        logger.error("Error in run test API: ", str(e))
        return create_failure(500, 'Error in run test API', 'Failed')
    return create_success('success')