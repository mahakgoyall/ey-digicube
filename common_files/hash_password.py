import jwt
from common_files.read_logger import get_logger
from common_files.read_configuration import read_config
from datetime import datetime, timedelta, timezone
import hashlib
import hmac
import base64



def create_hash_password(password: str) -> str:
    logger = get_logger()
    config = read_config()
    try:
        secret_key = config['HASH']['secret_key']
        message = bytes(password, 'utf-8')
        secret = bytes(secret_key, 'utf-8')
        signature = base64.b64encode(hmac.new(secret, message, digestmod=hashlib.sha256).digest())
        return signature.decode("utf-8")
    except Exception as e:
        logger.error(str(e))
        return 'False'



