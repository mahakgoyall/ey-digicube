import os
from datetime import datetime, timedelta
from azure.storage.blob import (
    BlobServiceClient,
    generate_blob_sas,
    ContainerClient,
)

from common_files.read_logger import get_logger


logger = get_logger()


CONTAINER_NAME = os.getenv('AZURE_STORAGE_CONTAINER_NAME') 


def upload_blob(local_file_name, blob_name, container_name=CONTAINER_NAME):
    try:
        if not is_blob_exist(blob_name=blob_name, container_name=container_name):
            upload_new_blob(local_file_name, blob_name, container_name=CONTAINER_NAME)
        return get_blob_sas_url(blob_name=blob_name, container_name=container_name)
    except Exception as e:
        logger.error("UploadBlobStorageError: "+str(e))
    return     

def upload_new_blob(local_file_name, blob_name, container_name=CONTAINER_NAME):
    try:
        blob_service_client = BlobServiceClient.from_connection_string(os.getenv('AZURE_STORAGE_CONNECTION_STRING'))
        blob_client = blob_service_client.get_blob_client(container=container_name, blob=blob_name)
        with open(local_file_name, 'rb') as data:
            blob_client.upload_blob(data)
        return True
    except Exception as e:
        logger.error("UploadNewBlobStorageError: "+str(e))
    return False

    

def get_blob_sas_url(blob_name, container_name=CONTAINER_NAME):
    try:
        account_name = os.getenv('AZURE_STORAGE_ACCOUNT_NAME')
        sas_token = generate_blob_sas(
                account_name=account_name, 
                container_name=container_name, 
                blob_name=blob_name, 
                account_key=os.getenv('AZURE_STORAGE_ACCOUNT_KEY'), 
                permission='r', 
                protocol='https',
                expiry=datetime.utcnow() + timedelta(hours=1)
            )
        return f'https://{account_name}.blob.core.windows.net/{container_name}/{blob_name}?{sas_token}'
    except Exception as e:
        logger.error("GetStorageSASError: "+str(e))
    return


def is_blob_exist(blob_name, container_name=CONTAINER_NAME):
    try:
        container_client = ContainerClient.from_connection_string(conn_str=os.getenv('AZURE_STORAGE_CONNECTION_STRING'), container_name=container_name)
        blob_list = container_client.list_blobs(name_starts_with=blob_name)
        for blob in blob_list:
            if blob.name == blob_name:
                return True
    except Exception as e:
        logger.error("CheckBlobExistsError: "+str(e))   
    return False

