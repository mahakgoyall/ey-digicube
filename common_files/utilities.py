import re

def split_file_path(file_path):
    return re.split('[\\\/]', str(file_path))