from django.http.response import JsonResponse
from common_files.read_logger import get_logger
from common_files.read_configuration import read_config
import pyodbc
import base64
config = read_config()
logger = get_logger()


def db_conn(database = base64.b64decode(str(config['DIGICUBEDB']['database_name'])).decode("utf-8"),commit=False):
    output = {
        'statusCode':400,
        'message': 'Error in db connection',
        'replyCode': 'Fail',
        'data': {}
    }
    try:
        driver = config['DIGICUBEDB']['driver']
        server = base64.b64decode(str(config['DIGICUBEDB']['server'])).decode("utf-8")
        uid = base64.b64decode(str(config['DIGICUBEDB']['username'])).decode("utf-8")
        password = base64.b64decode(str(config['DIGICUBEDB']['password'])).decode("utf-8")
        connection = pyodbc.connect("driver={};server={};database={};uid={};PWD={};Regional=No;".format(driver, server, database, uid, password),autocommit=commit)
        return connection
    except Exception as e:
        logger.error("Error in db connection: "+str(e))
        return JsonResponse(output)

